#include "red_black_tree.h"
#include "vector.h"

#include <stdio.h>
#include <string.h>

RedBlackTree *red_black_tree_create(size_t elementSize, RedBlackTreeComparator comp){
    RedBlackTree *result = NULL;
    result = malloc(sizeof(RedBlackTree));
    if(result == NULL)
        return NULL;
    result->root = NULL;
    result->elementSize = elementSize;
    result->comparator = comp;
    return result;
}

RedBlackTreeNode* red_black_tree_node_create()
{
    RedBlackTreeNode *node = NULL;
    node = malloc(sizeof(RedBlackTreeNode));
    if(node == NULL)
        return NULL;
    node->color = RED_BLACK_TREE_RED;
    node->data = NULL;
    node->left = NULL;
    node->right = NULL;
    node->parent = NULL;
    return node;
}


static void left_rotate(RedBlackTree *tree, RedBlackTreeNode *node)
{
    RedBlackTreeNode *newParent = node->right;
    node->right = newParent->left;
    if(newParent->left != NULL)
    {
        node->right->parent = node;
    }
    newParent->left = node;
    if(node->parent == NULL)
    {
        tree->root = newParent;
    }
    else
    {
        if(node->parent->left == node)
            node->parent->left = newParent;
        else
            node->parent->right = newParent;
    }
    newParent->parent = node->parent;
    node->parent = newParent;
}

static void right_rotate(RedBlackTree *tree, RedBlackTreeNode *node)
{
    RedBlackTreeNode *newParent = node->left;
    node->left = newParent->right;
    if(newParent->right != NULL)
    {
        node->left->parent = node;
    }
    newParent->right = node;
    if(node->parent == NULL)
    {
        tree->root = newParent;
    }
    else
    {
        if(node->parent->left == node)
            node->parent->left = newParent;
        else
            node->parent->right = newParent;
    }
    newParent->parent = node->parent;
    node->parent = newParent;
}

char red_black_tree_insert(RedBlackTree *tree, void *value)
{
    RedBlackTreeNode *node = red_black_tree_node_create();
    if(node == NULL)
        return RED_BLACK_TREE_NODE_CREATE_FAILURE;
    node->data = malloc(tree->elementSize);
    if(node->data == NULL)
        return RED_BLACK_TREE_DATA_ALLOC_FAILURE;
    memcpy(node->data, value, tree->elementSize);
    if(tree->root == NULL)
    {
        tree->root = node;
        node->color = RED_BLACK_TREE_BLACK;
        return RED_BLACK_TREE_VALUE_INSERTED;
    }
    RedBlackTreeNode *current = tree->root;
    while(1)
    {
        char result = tree->comparator(current->data, value);
        if(!result)
            return 0;
        if(result < 0)
        {
            // value in tree is smaller than new value
            if(current->right == NULL)
            {
                current->right = node;
                node->parent = current;
                break;
            }
            else
            {
                current = current->right;
            }
        }
        else
        {
            // value in tree is larger than new value
            if(current->left == NULL)
            {
                current->left = node;
                node->parent = current;
                break;
            }
            else
            {
                current = current->left;
            }
        }
    }
    RedBlackTreeNode *uncle = NULL;
    while(node != tree->root && node->parent->color == RED_BLACK_TREE_RED)
    {
        if(node->parent == node->parent->parent->left)
        {
            uncle = node->parent->parent->right;
            if(uncle == NULL || uncle->color == RED_BLACK_TREE_BLACK)
            {
                if(node == node->parent->right)
                {
                    node = node->parent;
                    left_rotate(tree, node);
                }
                node->parent->color = RED_BLACK_TREE_BLACK;
                node->parent->parent->color = RED_BLACK_TREE_RED;
                right_rotate(tree, node->parent->parent);
            }
            else
            {
                node->parent->color = RED_BLACK_TREE_BLACK;
                uncle->color = RED_BLACK_TREE_BLACK;
                node->parent->parent->color = RED_BLACK_TREE_RED;
                node = node->parent->parent;
            }
        }
        else if(node->parent == node->parent->parent->right)
        {
            uncle = node->parent->parent->left;
            if(uncle == NULL || uncle->color == RED_BLACK_TREE_BLACK)
            {
                if(node == node->parent->left)
                {
                    node = node->parent;
                    right_rotate(tree, node);
                }
                node->parent->color = RED_BLACK_TREE_BLACK;
                node->parent->parent->color = RED_BLACK_TREE_RED;
                left_rotate(tree, node->parent->parent);
            }
            else
            {
                node->parent->color = RED_BLACK_TREE_BLACK;
                uncle->color = RED_BLACK_TREE_BLACK;
                node->parent->parent->color = RED_BLACK_TREE_RED;
                node = node->parent->parent;
            }
        }
    }
    tree->root->color = RED_BLACK_TREE_BLACK;
    return RED_BLACK_TREE_VALUE_INSERTED;
}

static void red_black_tree_node_free_descendants(RedBlackTreeNode *node)
{
    if(node->left != NULL)
        red_black_tree_node_free_descendants(node->left);
    if(node->right != NULL)
        red_black_tree_node_free_descendants(node->right);
    free(node->data);
    free(node);
}

void red_black_tree_free(RedBlackTree *tree)
{
    if(tree->root == NULL)
        return;
    red_black_tree_node_free_descendants(tree->root);
    free(tree);
}

void red_black_tree_free_data(RedBlackTree *tree)
{
    if(tree->root == NULL)
        return;
    red_black_tree_node_free_descendants(tree->root);
}

static char red_black_tree_node_contains(RedBlackTreeNode *node, void *elem, size_t elementSize, RedBlackTreeComparator comp)
{
    char compRes = comp(elem, node->data);
    if(compRes == 0)
        return 1;
    if(compRes > 0)
    {
        if(node->right == NULL)
            return 0;
        return red_black_tree_node_contains(node->right, elem, elementSize, comp);
    }
    if(node->left == NULL)
        return 0;
    return red_black_tree_node_contains(node->left, elem, elementSize, comp);
}

char red_black_tree_contains(RedBlackTree *tree, void *elem)
{
    if(tree->root == NULL)
        return 0;
    return red_black_tree_node_contains(tree->root, elem, tree->elementSize, tree->comparator);
}

RedBlackTreeNode* red_black_tree_find(RedBlackTreeNode *node, void *elem, RedBlackTreeComparator comp)
{
    if(node == NULL)
        return NULL;
    char compRes = comp(elem, node->data);
    if(compRes == 0)
        return node;
    if(compRes > 0)
        return red_black_tree_find(node->right, elem, comp);
    else
        return red_black_tree_find(node->left, elem, comp);
}

RedBlackTreeNode* red_black_tree_get_largest_node(RedBlackTreeNode *node)
{
    if(node->right == NULL)
        return node;
    return red_black_tree_get_largest_node(node->right);
}

static void red_black_tree_delete_fixup(RedBlackTree *tree, RedBlackTreeNode *x, RedBlackTreeNode *xParent)
{
    RedBlackTreeNode *w = NULL;
    if(x != NULL)
        xParent = x->parent;
    while((x == NULL || x->color == RED_BLACK_TREE_BLACK) && x != tree->root)
    {
        if(x == xParent->left)
        {
            w = xParent->right;
            if(w->color == RED_BLACK_TREE_RED)
            {
                w->color = RED_BLACK_TREE_BLACK;
                xParent->color = RED_BLACK_TREE_RED;
                left_rotate(tree, xParent);
            }
            else if((w->left == NULL || w->left->color == RED_BLACK_TREE_BLACK) && (w->right == NULL || w->right->color == RED_BLACK_TREE_BLACK))
            {
                w->color = RED_BLACK_TREE_RED;
                x = xParent;
                xParent = x->parent;
            }
            else 
            {
                if((w->left != NULL && w->left->color == RED_BLACK_TREE_RED) && (w->right == NULL || w->right->color == RED_BLACK_TREE_BLACK))
                {
                    w->left->color = RED_BLACK_TREE_BLACK;
                    w->color = RED_BLACK_TREE_RED;
                    right_rotate(tree, w);
                    w = xParent->right;
                }
                w->color = xParent->color;
                xParent->color = RED_BLACK_TREE_BLACK;
                w->right->color = RED_BLACK_TREE_BLACK;
                left_rotate(tree, xParent);
                return;
            }
        }
        else
        {
            w = xParent->left;
            if(w->color == RED_BLACK_TREE_RED)
            {
                w->color = RED_BLACK_TREE_BLACK;
                xParent->color = RED_BLACK_TREE_RED;
                right_rotate(tree, xParent);
            }
            else if((w->left == NULL || w->left->color == RED_BLACK_TREE_BLACK) && (w->right == NULL || w->right->color == RED_BLACK_TREE_BLACK))
            {
                w->color = RED_BLACK_TREE_RED;
                x = xParent;
                xParent = x->parent;
            }
            else 
            {
                if((w->right != NULL && w->right->color == RED_BLACK_TREE_RED) && (w->left == NULL || w->left->color == RED_BLACK_TREE_BLACK))
                {
                    w->right->color = RED_BLACK_TREE_BLACK;
                    w->color = RED_BLACK_TREE_RED;
                    left_rotate(tree, w);
                    w = xParent->left;
                }
                w->color = xParent->color;
                xParent->color = RED_BLACK_TREE_BLACK;
                w->left->color = RED_BLACK_TREE_BLACK;
                right_rotate(tree, xParent);
                return;
            }

        }
    }
    x->color = RED_BLACK_TREE_BLACK;
}

char red_black_tree_delete(RedBlackTree *tree, void *elem)
{
    if(tree->root == NULL)
        return 0;
    RedBlackTreeNode *nodeToDelete = red_black_tree_find(tree->root, elem, tree->comparator);
    if(nodeToDelete == NULL)
        return 0;
    RedBlackTreeNode *replacement = NULL;
    RedBlackTreeNode *x = NULL;
    RedBlackTreeNode *xParent = NULL;
    if(nodeToDelete->left == NULL)
    {
        replacement = nodeToDelete->right;
        if(replacement != NULL)
        {
            replacement->parent = nodeToDelete->parent;
            x = replacement;
        }
        else
        {
            xParent = nodeToDelete->parent;
        }
    }
    else if(nodeToDelete->right == NULL)
    {
        replacement = nodeToDelete->left;
        replacement->parent = nodeToDelete->parent;
        x = replacement;
    }
    else
    {
        replacement = red_black_tree_get_largest_node(nodeToDelete->left);
        x = replacement->left;
        if(replacement == nodeToDelete->left)
        {
            replacement->parent = nodeToDelete->parent;
            replacement->right = nodeToDelete->right;
            if(replacement->left == NULL)
            {
                xParent = replacement;
            }
            nodeToDelete->right->parent = replacement;
        }
        else
        {
            if(replacement->left == NULL)
            {
                xParent = replacement->parent;
            }
            replacement->parent->right = replacement->left;
            if(replacement->left != NULL)
                replacement->left->parent = replacement->parent;
            replacement->left = nodeToDelete->left;
            replacement->right = nodeToDelete->right;
            replacement->parent = nodeToDelete->parent;
            nodeToDelete->left->parent = replacement;
            nodeToDelete->right->parent = replacement;
        }
    }
    if(nodeToDelete == tree->root)
        tree->root = replacement;
    else if(nodeToDelete->parent->right == nodeToDelete)
        nodeToDelete->parent->right = replacement;
    else
        nodeToDelete->parent->left = replacement;
    if(nodeToDelete->color == RED_BLACK_TREE_RED && replacement != NULL && replacement->color == RED_BLACK_TREE_BLACK)
    {
        replacement->color = RED_BLACK_TREE_RED;
        red_black_tree_delete_fixup(tree, x, xParent);
    }
    else if(nodeToDelete->color == RED_BLACK_TREE_BLACK && replacement != NULL && replacement->color == RED_BLACK_TREE_RED)
    {
        replacement->color = RED_BLACK_TREE_BLACK;
    }
    else if(nodeToDelete->color == RED_BLACK_TREE_BLACK && (replacement == NULL || replacement->color == RED_BLACK_TREE_BLACK))
    {
        red_black_tree_delete_fixup(tree, x, xParent);
    }
    free(nodeToDelete->data);
    free(nodeToDelete);
    return RED_BLACK_TREE_VALUE_REMOVED;
}

static void red_black_tree_node_inorder(RedBlackTreeNode *node, RedBlackTreeClosure *closure)
{
    if(node->left != NULL)
        red_black_tree_node_inorder(node->left, closure);
    closure->action(node, closure->environment);
    if(node->right != NULL)
        red_black_tree_node_inorder(node->right, closure);
}

void red_black_tree_for_each_inorder(RedBlackTree *tree, RedBlackTreeClosure *closure)
{
    if(tree->root == NULL)
        return;
    red_black_tree_node_inorder(tree->root, closure);
}

static void red_black_tree_node_preorder(RedBlackTreeNode *node, RedBlackTreeClosure *closure)
{
    closure->action(node, closure->environment);
    if(node->left != NULL)
        red_black_tree_node_preorder(node->left, closure);
    if(node->right != NULL)
        red_black_tree_node_preorder(node->right, closure);
}

void red_black_tree_for_each_preorder(RedBlackTree *tree, RedBlackTreeClosure *closure)
{
    if(tree->root == NULL)
        return;
    red_black_tree_node_preorder(tree->root, closure);
}

char* red_black_tree_to_mermaid_string(RedBlackTree *tree, RedBlackTreeContentToString toString)
{
    if(tree->root == NULL)
        return 0;
    char *res;
    size_t len;
    FILE *stream = open_memstream(&res, &len);
    Vector todo = vector_init(sizeof(RedBlackTreeNode*));
    size_t counter = 0;
    fprintf(stream, "graph TD\nclassDef redNode fill:#ff0000,color:#ffffff;\nclassDef blackNode fill:#000000,color:#ffffff;\n");
    vector_push(&todo, &tree->root);
    char *str;
    char *class;
    while(!vector_is_empty(&todo))
    {
        RedBlackTreeNode *tmp;
        vector_poll(&todo, &tmp);
        str  = toString(tmp->data);
        class = (tmp->color == RED_BLACK_TREE_RED) ? "redNode" : "blackNode";
        if(tmp->left == NULL && tmp->right == NULL)
        {
            fprintf(stream, "%lu((%s)):::%s\n", counter, str, class);
        }
        if(tmp->left != NULL)
        {
            fprintf(stream, "%lu((%s)):::%s---%lu\n", counter, str, class, counter + todo.size + 1);
            vector_push(&todo, &tmp->left);
        }
        if(tmp->right != NULL)
        {
            fprintf(stream, "%lu((%s)):::%s---%lu\n", counter, str, class, counter + todo.size + 1);
            vector_push(&todo, &tmp->right);
        }
        counter++;
        free(str);
    }
    fclose(stream);
    free(todo.data);
    return res;
}

RBTInorderIterator* rbt_inorder_iterator_create(RedBlackTree *tree)
{
    RBTInorderIterator *res = malloc(sizeof(RBTInorderIterator));
    if(res == NULL)
        return NULL;
    res->stack = vector_init(sizeof(RedBlackTreeNode*));
    RedBlackTreeNode *current = tree->root;
    while(current != NULL)
    {
        vector_push(&res->stack, &current);
        current = current->left;
    }
    return res;
}

char rbt_inorder_iterator_has_next(RBTInorderIterator *it)
{
    if(vector_is_empty(&it->stack))
        return 0;
    RedBlackTreeNode *parent = *(void**)vector_element_at(&it->stack, 0);
    if(it->stack.size == 1)
        return parent->right != NULL;
    RedBlackTreeNode *child = NULL;
    for(size_t i = 1; i < it->stack.size - 1; i++)
    {
        child = *(void**)vector_element_at(&it->stack, i);
        if(parent->left == child)
            return 1;
        parent = child;
    }
    return 0;
}

void rbt_inorder_iterator_next(RBTInorderIterator *it)
{
    RedBlackTreeNode *current = *(void**)vector_peek(&it->stack);
    if(current->right != NULL)
    {
        current = current->right;
        while(current != NULL)
        {
            vector_push(&it->stack, &current);
            current = current->left;
        }
    }
    else
    {
        RedBlackTreeNode *old = current;
        vector_pop(&it->stack, NULL);
        if(it->stack.size == 0)
            return;
        current = *(void**)vector_peek(&it->stack);
        while(1)
        {
            if(current->left == old)
                break;
            vector_pop(&it->stack, NULL);
            if(it->stack.size == 0)
                break;
            old = current;
            current = *(void**)vector_peek(&it->stack);
        }
    }
}

RedBlackTreeNode *rbt_inorder_iterator_get(RBTInorderIterator *it)
{
    if(it->stack.size == 0)
        return NULL;
    return *(void**)vector_peek(&it->stack);
}

void rbt_inorder_iterator_free(RBTInorderIterator *it)
{
    free(it->stack.data);
    free(it);
}
