#ifndef LEXGEN_FILE_PARSER_H
#define LEXGEN_FILE_PARSER_H

#include "vector.h"
#include "nfa.h"
#include <stdlib.h>

typedef struct FileParseResult {
    Vector modes;
    Vector lines;
    Vector errors;
    char *input;
} FileParseResult;

typedef struct FileParser {
    Vector modes;
    size_t pos;
    unsigned int ruleNo;
    char *input;
} FileParser;

typedef struct Line {
    NFA nfa;
    Vector modes;
    struct {
        size_t index;
        size_t len;
    } codeSlice;
} Line;

typedef struct FileParseError {
    char *msg;
    size_t line;
    size_t col;
} FileParseError;

FileParseResult parse_file(char*);

Vector parse_lines(FileParser*);

Line parse_line(FileParser*);

#endif