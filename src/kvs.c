#include "kvs.h"
#include <string.h>
#include <limits.h>

KeyValueStore kvs_init(HashFunction hashFunc, size_t elementSize)
{
    KeyValueStore set;
    set.elementSize = elementSize;
    set.size = 0;
    set.capacity = 10;
    set.data = malloc((set.elementSize + sizeof(size_t))*set.capacity);
    set.hashFunc = hashFunc;
    memset(set.data, 255, set.capacity * (sizeof(size_t) + set.elementSize));
    return set;
}

KeyValueStore kvs_init_cap(HashFunction hashFunc, size_t elementSize, size_t capacity)
{
    KeyValueStore set;
    set.elementSize = elementSize;
    set.size = 0;
    set.capacity = capacity;
    set.data = malloc((set.elementSize + sizeof(size_t))*set.capacity);
    set.hashFunc = hashFunc;
    memset(set.data, 255, set.capacity * (sizeof(size_t) + set.elementSize));
    return set;
}

void kvs_free(KeyValueStore *set)
{
    free(set->data);
}

static void kvs_reinsert(KeyValueStore *set, size_t key, void *value)
{
    size_t slot = key % set->capacity;
    void *location;
    char keyExists = 0;
    do {
        location = set->data + slot * (set->elementSize + sizeof(size_t));
        if(*(size_t*)location == (size_t)-1)
        {
            break;
        }
        else if(*(size_t*)location == key)
        {
            keyExists = 1;
            break;
        }
        slot = (slot + 1) % set->capacity;
    } while(1);
    if(!keyExists)
        memcpy(location, &key, sizeof(size_t));
    memcpy(location + sizeof(size_t), value, set->elementSize);
}

char kvs_insert(KeyValueStore *set, void *keyElem, void *value)
{
    size_t key = set->hashFunc(keyElem);
    if(key == (size_t)-1)
        return KVS_KEY_TOO_BIG;
    if(set->size == set->capacity)
    {
        set->capacity *= 2;
        void *newData = malloc(set->capacity * (sizeof(size_t) + set->elementSize));
        if(newData == NULL)
            return KVS_INSERT_REALLOC_FAILED;
        memset(newData, 255, set->capacity *(sizeof(size_t) + set->elementSize));
        void *oldData = set->data;
        set->data = newData;
        size_t copies = 0;
        for(size_t i = 0; i < set->size; i++)
        {
            size_t keyValue = *(size_t*)(oldData + i * (sizeof(size_t) + set->elementSize));
            if(keyValue == (size_t)-1)
            {
                continue;
            }
            kvs_reinsert(set, keyValue, oldData + i * (sizeof(size_t) + set->elementSize) + sizeof(size_t));
            copies++;
            if(copies == set->size)
                break;
        }
        free(oldData);
    }
    size_t slot = key % set->capacity;
    void *location;
    char keyExists = 0;
    do {
        location = set->data + slot * (set->elementSize + sizeof(size_t));
        if(*(size_t*)location == (size_t)-1)
        {
            break;
        }
        else if(*(size_t*)location == key)
        {
            keyExists = 1;
            break;
        }
        slot = (slot + 1) % set->capacity;
    } while(1);
    if(!keyExists)
        memcpy(location, &key, sizeof(size_t));
    memcpy(location + sizeof(size_t), value, set->elementSize);
    set->size++;
    return KVS_INSERT_SUCCESS;
}

void *kvs_get(KeyValueStore *set, void *keyElem)
{
    size_t key = set->hashFunc(keyElem);
    if(key == (size_t)-1)
        return NULL;
    size_t slot = key % set->capacity;;
    size_t tries = 0;
    void *location;
    do {
        location = set->data + slot * (set->elementSize + sizeof(size_t));
        if(*(size_t*)location == key)
        {
            return location + sizeof(size_t);
        }
        else if(*(size_t*)location == (size_t)-1)
        {
            return NULL;
        }
        slot = (slot + 1) % set->capacity;
        tries++;
    } while(tries < set->capacity);
    return NULL;
}