#ifndef LEXGEN_NFA_H
#define LEXGEN_NFA_H

#include <stdlib.h>
#include "set.h"
#include "vector.h"

#define EPSILON_TRANSITION -1

typedef struct NFAState NFAState;

typedef struct NFA NFA;

typedef void (*NFAStateAction)(NFAState*, void*);

typedef struct NFA {
    unsigned int count;
    unsigned int groupCount;
    NFAState *start;
    NFAState *final;
} NFA;

typedef struct NFAState {
    unsigned int id;
    unsigned int accept;
    Vector groupStart;
    Vector groupAccept;
    Vector transitions;
    long long modeStart;
    long long modeEnd;
    char start;
} NFAState;

typedef struct NFAStateTransition {
    Set characters;
    char inverted;
    NFAState *target;
} NFAStateTransition;

NFA nfa_init();

NFA *nfa_create();

void nfa_free(NFA*);

NFAStateTransition *nfa_init_statetransition();

NFAState* nfa_create_state(NFA*);

NFAState* nfa_state_shallow_copy(NFAState*);

void nfa_add_state_transition(NFAState*, NFAStateTransition*);

void nfa_remove_state_transition(NFAState*, NFAStateTransition*);

void nfa_update_ids(NFA*, unsigned int);

void nfa_update_state_and_group_ids(NFA*, unsigned int, unsigned int);

NFA nfa_deep_copy(NFA *);

NFA nfa_create_char(char c);

NFA nfa_create_from_chars(char *, size_t);

NFA nfa_create_char_inverted(char c);

NFA nfa_create_from_chars_inverted(char*, size_t);

NFA nfa_create_concat(NFA*, NFA*);

NFA nfa_create_choice(NFA*, NFA*);

NFA nfa_create_option(NFA*);

NFA nfa_create_kleene(NFA*);

NFA nfa_create_oneormore(NFA*);

Vector nfa_linearize(NFA*);

char nfa_compare_states(NFAState*, NFAState*);

char *nfa_to_mermaid_string(NFA*);

void nfa_for_each(NFA*, NFAStateAction, void*);

void nfa_for_each_bf(NFA*, NFAStateAction, void*);

void nfa_for_each_sorted(NFA*, NFAStateAction, void*);

#endif