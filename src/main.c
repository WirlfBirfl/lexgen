#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "util.h"
#include "regex_parser.h"
#include "nfa.h"
#include "vector.h"

Either read_file(char* file);

int main(int argc, char **argv)
{
    char *fileName;
    if(argc == 1)
    {
        // no input file given
        fileName = "regex.lex";
    }
    else if(argc >= 2)
    {
        // input file given
        fileName = argv[1];
    }
    Either result = read_file(fileName);
    if(result.constr == Either_Failure)
    {
        printf("%s\n", result.content.errorMsg);
        return 1;
    }
    FileParseResult parseResult = parse_file((char*)result.content.data);
    if(parseResult.errors.size > 0)
    {
        for(size_t i = 0; i < parseResult.errors.size; i++)
        {
            FileParseError *err = vector_element_at(&parseResult.errors, i);
            printf("Error in line %zu col %zu: %s", err->line, err->col, err->msg);
        }
        return -1;
    }
    VECTOR_FOR_I(parseResult.lines, Line,{
        char *mermaid = nfa_to_mermaid_string(&elem->nfa);
        printf("%s\n\n", mermaid);
        free(mermaid);
    })
    return 0;
}

Either read_file(char *filePath)
{
    struct stat buffer;
    int statResult = stat(filePath, &buffer);
    size_t n = 0;
    Either result;
    if(statResult == -1)
    {
        result.constr = Either_Failure;
        result.content.errorMsg = "Could not get file stat.";
        return result;
    }
    FILE *f = fopen(filePath, "r");
    if(f == NULL)
    {
        result.constr = Either_Failure;
        result.content.errorMsg = "Could not open file.";
        return result;
    }
    char *fileContent = malloc((buffer.st_size + 1 )* sizeof(char));
    int c;
    while((c = fgetc(f)) != EOF)
    {
        fileContent[n++] = (char) c;
    }
    fileContent[n] = EOF;
    result.constr = Either_Success;
    result.content.data = fileContent;
    return result;
}