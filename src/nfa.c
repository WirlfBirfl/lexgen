#include <string.h>
#include <stdio.h>

#include "nfa.h"
#include "nfa_util.h"
#include "vector.h"
#include "regex_parser.h"
#include "set.h"

NFA* nfa_create()
{
    NFA *nfa = malloc(sizeof(NFA));
    nfa->count = 0;
    nfa->start = NULL;
    nfa->final = NULL;
    nfa->groupCount = 0;
    return nfa;
}

void nfa_free(NFA *nfa)
{
    // TODO: do this
    free(nfa);
}

NFA nfa_init()
{
    NFA nfa;
    nfa.count = 0;
    nfa.start = NULL;
    nfa.final = NULL;
    nfa.groupCount = 0;
    return nfa;
}

NFAState* nfa_create_state(NFA *nfa)
{
    NFAState *newState = (NFAState*)malloc(sizeof(NFAState));
    newState->accept = 0;
    newState->id = nfa->count;
    nfa->count++;
    newState->transitions = vector_init(sizeof(NFAStateTransition));
    newState->start = 0;
    newState->groupStart = vector_init(sizeof(unsigned int));
    newState->groupAccept = vector_init(sizeof(unsigned int));
    return newState;
}

NFA nfa_create_char(char c)
{
    NFA result = nfa_init();
    result.start = nfa_create_state(&result);
    result.final = nfa_create_state(&result);
    result.start->start = 1;
    result.final->accept = 1;
    NFAStateTransition *transition = nfa_init_statetransition();
    set_insert(&transition->characters, &c);
    transition->target = result.final;
    nfa_add_state_transition(result.start, transition);
    free(transition);
    return result;
}

NFA nfa_create_char_inverted(char c)
{
    NFA ret = nfa_create_char(c);
    NFAStateTransition *t = vector_peek(&ret.start->transitions);
    t->inverted = 1;
    return ret;
}

NFA nfa_create_from_chars(char *chars, size_t len)
{
    NFA result = nfa_init();
    result.start = nfa_create_state(&result);
    result.final = nfa_create_state(&result);
    result.start->start = 1;
    result.final->accept = 1;
    NFAStateTransition *transition = nfa_init_statetransition();
    set_insert_all(&transition->characters, chars, len);
    transition->target = result.final;
    nfa_add_state_transition(result.start, transition);
    free(transition);
    return result;
}

NFA nfa_create_from_chars_inverted(char *chars, size_t len)
{
    NFA ret = nfa_create_from_chars(chars, len);
    NFAStateTransition *t = vector_peek(&ret.start->transitions);
    t->inverted = 1;
    return ret;
}

NFA nfa_create_concat(NFA *fst, NFA *scd)
{
    NFA result = nfa_init();
    result.start = fst->start;
    result.final = scd->final;
    fst->final->accept = 0;
    scd->start->start = 0;
    result.groupCount = fst->groupCount + scd->groupCount;
    result.count = fst->count + scd->count;
    nfa_update_state_and_group_ids(scd, fst->count, fst->groupCount);
    NFAStateTransition *concatEpsilon = nfa_init_statetransition();
    char EPSILON = EPSILON_TRANSITION;
    set_insert(&concatEpsilon->characters, &EPSILON);
    concatEpsilon->target = scd->start;
    nfa_add_state_transition(fst->final, concatEpsilon);
    free(concatEpsilon);
    return result;
}

NFA nfa_create_choice(NFA *fst, NFA *scd)
{
    if(fst->count == 0 && scd->count != 0)
    {
        return nfa_deep_copy(scd);
    }
    else if(fst->count != 0 && scd->count == 0)
    {
        return nfa_deep_copy(fst);
    }
    else if(fst->count == 0 && scd->count == 0)
    {
        return nfa_init();
    }
    char EPSILON = EPSILON_TRANSITION;
    NFA result = nfa_init();
    // first state with epsilon transitions to
    // each start state of the sub expressions
    result.start = nfa_create_state(&result);
    result.start->start = 1;
    result.groupCount = fst->groupCount + scd->groupCount;
    NFAStateTransition *epsilon1 = nfa_init_statetransition();
    set_insert(&epsilon1->characters, &EPSILON);
    epsilon1->target = fst->start;
    NFAStateTransition *epsilon2 = nfa_init_statetransition();
    set_insert(&epsilon2->characters, &EPSILON);
    epsilon2->target = scd->start;
    nfa_add_state_transition(result.start, epsilon1);
    nfa_add_state_transition(result.start, epsilon2);
    // update ids in subexpressions
    nfa_update_ids(fst, 1);
    result.count += fst->count;
    // update ids of second subexpression
    nfa_update_state_and_group_ids(scd, result.count, fst->groupCount);
    result.count += scd->count;
    // create final state 
    result.final = nfa_create_state(&result);
    result.final->accept = 1;
    // make start and final states of subexpressions normal states
    fst->start->start = 0;
    fst->final->accept = 0;
    scd->start->start = 0;
    scd->final->accept = 0;
    NFAStateTransition *epsilon3 = nfa_init_statetransition();
    set_insert(&epsilon3->characters, &EPSILON);
    epsilon3->target = result.final;
    NFAStateTransition *epsilon4 = nfa_init_statetransition();
    set_insert(&epsilon4->characters, &EPSILON);
    epsilon4->target = result.final;
    nfa_add_state_transition(fst->final, epsilon3);
    nfa_add_state_transition(scd->final, epsilon4);
    // clean up memory mess
    free(epsilon1);
    free(epsilon2);
    free(epsilon3);
    free(epsilon4);
    return result;
}

NFA nfa_create_option(NFA *nfa)
{
    char EPSILON = EPSILON_TRANSITION;
    NFA result = nfa_init();
    result.start = nfa_create_state(&result);
    result.groupCount = nfa->groupCount;
    result.start->start = 1;
    nfa_update_ids(nfa, 1);
    NFAStateTransition *epsilon1 = nfa_init_statetransition();
    set_insert(&epsilon1->characters, &EPSILON);
    epsilon1->target = nfa->start;
    nfa_add_state_transition(result.start, epsilon1);
    result.count += nfa->count;
    result.final = nfa_create_state(&result);
    result.final->accept = 1;
    NFAStateTransition *epsilon2 = nfa_init_statetransition();
    set_insert(&epsilon2->characters, &EPSILON);
    epsilon2->target = result.final;
    nfa_add_state_transition(result.start, epsilon2);
    NFAStateTransition *epsilon3 = nfa_init_statetransition();
    set_insert(&epsilon3->characters, &EPSILON);
    epsilon3->target = result.final;
    nfa_add_state_transition(nfa->final, epsilon3);
    nfa->start->start = 0;
    nfa->final->accept = 0;
    free(epsilon1);
    free(epsilon2);
    free(epsilon3);
    return result;
}

NFA nfa_create_kleene(NFA *nfa)
{
    char EPSILON = EPSILON_TRANSITION;
    NFA result = nfa_create_option(nfa);
    result.groupCount = nfa->groupCount;
    NFAStateTransition *epsilon = nfa_init_statetransition();
    set_insert(&epsilon->characters, &EPSILON);
    epsilon->target = result.start;
    nfa_add_state_transition(result.final, epsilon);
    free(epsilon);
    return result;
}

NFA nfa_create_oneormore(NFA *nfa)
{
    NFA result = nfa_create_kleene(nfa);
    result.groupCount = nfa->groupCount;
    NFAStateTransition *elem = nfa_init_statetransition();
    elem->target = result.final;
    nfa_remove_state_transition(result.start, elem);
    return result;
}

static char compare_chars(void *fst, void *scd)
{
    char a = *(char*)fst;
    char b = *(char*)scd;
    return a -b;
}

NFAStateTransition* nfa_init_statetransition()
{
    NFAStateTransition *result = malloc(sizeof(NFAStateTransition));
    result->characters = set_init(compare_chars, sizeof(char));
    result->target = NULL;
    result->inverted = 0;
    return result;
}

long compare_statetransitions(NFAStateTransition *fst, NFAStateTransition *scd)
{
    if(fst == NULL && scd == NULL)
        return 0;
    if(fst != NULL && scd == NULL)
        return 1;
    if(fst == NULL && scd != NULL)
        return -1;
    if(!set_is_equal(&fst->characters, &scd->characters))
        return 1;
    if(fst->target != scd->target)
        return 1;
    return 0;
}

void nfa_remove_state_transition(NFAState *state, NFAStateTransition *transition)
{
    vector_remove_element(&state->transitions, transition, (VectorElementComparator)compare_statetransitions);
}

void nfa_add_state_transition(NFAState *state, NFAStateTransition *transition)
{
    vector_push(&state->transitions, transition);
}

void nfa_update_ids(NFA *nfa, unsigned int offset)
{
    Vector linearized = nfa_linearize(nfa);
    for(size_t i=0; i<linearized.size; i++)
    {
        NFAState *tmp = *(NFAState**)vector_element_at(&linearized, i);
        tmp->id += offset;
    }
    free(linearized.data);
}

void nfa_update_state_and_group_ids(NFA *nfa , unsigned int stateIdOffset, unsigned int groupNoOffset)
{
    Vector linearized = nfa_linearize(nfa);
    for(size_t i=0; i<linearized.size; i++)
    {
        NFAState *tmp = *(NFAState**)vector_element_at(&linearized, i);
        tmp->id += stateIdOffset;
        for(size_t i = 0; i < tmp->groupAccept.size; i++)
        {
            unsigned int *g = vector_element_at(&tmp->groupAccept, i);
            *g = *g + groupNoOffset;
        }
        for(size_t i = 0; i < tmp->groupStart.size; i++)
        {
            unsigned int *g = vector_element_at(&tmp->groupStart, i);
            *g = *g + groupNoOffset;

        }
    }
    free(linearized.data);
}


static long compare_pointers(void *fst, void *scd)
{
    // necessary to see if a pointer in the list points to the same object
    // since void* points to the list *(void**) dereferences the pointer at that location
    // isnt C fun?
    return (fst == *(void**)scd) ? 0 : 1;
}

Vector nfa_linearize(NFA *nfa)
{
    Vector todo = vector_init(sizeof(NFAState*));
    Vector done = vector_init(sizeof(NFAState*));
    vector_push(&todo, &nfa->start);
    while(!vector_is_empty(&todo))
    {
        NFAState *tmp;
        vector_poll(&todo, &tmp);
        vector_push(&done, &tmp);
        for(size_t i=0; i<tmp->transitions.size; i++)
        {
            NFAStateTransition *trans = vector_element_at(&tmp->transitions, i);
            if(vector_contains(&todo, trans->target, compare_pointers) < 0 && vector_contains(&done, trans->target, compare_pointers) < 0)
            {
                vector_push(&todo, &trans->target);
            }
        }
    }
    free(todo.data);
    return done;
}

long nfa_state_compare_based_on_id(void *state, void *other)
{
    NFAState *scd = *(void**)other;
    NFAState *fst = (NFAState*)state;
    return fst->id - scd->id;
}

long nfa_state_ordering(void *state, void *other)
{
    NFAState *scd = *(void**)other;
    NFAState *fst = *(void**)state;
    return fst->id == scd->id ? 0 : fst->id > scd->id ? 1 : -1; 
}

NFA nfa_deep_copy(NFA *other)
{
    NFA copy;
    copy.count = other->count;
    copy.start = nfa_state_shallow_copy(other->start);
    copy.groupCount = other->groupCount;
    Vector todo = vector_init(sizeof(NFAState*));
    Vector done = vector_init(sizeof(NFAState*));
    vector_push(&todo, &copy.start);
    while(!vector_is_empty(&todo))
    {
        NFAState *tmp;
        vector_pop(&todo, &tmp);
        vector_push(&done, &tmp);
        if(tmp->accept)
            copy.final = tmp;
        for(size_t i = 0; i < tmp->transitions.size; i++)
        {
            NFAStateTransition *trans = vector_element_at(&tmp->transitions, i);
            long index = vector_contains(&todo, trans->target, nfa_state_compare_based_on_id);
            if(index >= 0)
            {
                trans->target = *(void**)vector_element_at(&todo, index);
                continue;
            }
            index = vector_contains(&done, trans->target, nfa_state_compare_based_on_id);
            if(index >= 0)
            {
                trans->target = *(void**)vector_element_at(&done, index);
                continue;
            }
            NFAState *copied = nfa_state_shallow_copy(trans->target);
            trans->target = copied;
            vector_push(&todo, &copied);
        }
    }
    free(todo.data);
    free(done.data);
    return copy;
}

NFAState *nfa_state_shallow_copy(NFAState *other)
{
    NFAState *copy = malloc(sizeof(NFAState));
    copy->id = other->id;
    copy->accept = other->accept;
    copy->start = other->start;
    copy->transitions = vector_copy(&other->transitions);
    copy->groupStart = vector_copy(&other->groupStart);
    copy->groupAccept = vector_copy(&other->groupAccept);
    return copy;
}

long compare_unsigned_ints(void *fst, void *scd)
{
    unsigned int fstInt = *(unsigned int*)fst; 
    unsigned int scdInt = *(unsigned int*)scd;
    return  (long)fstInt - (long)scdInt;
}

void nfa_for_each(NFA *nfa, NFAStateAction action, void* environment)
{
    Vector visited = vector_init(sizeof(unsigned int));
    Vector toVisit = vector_init(sizeof(NFAState*));
    vector_push(&toVisit, &nfa->start);
    while(!vector_is_empty(&toVisit))
    {
        NFAState *current;
        vector_pop(&toVisit, &current);
        action(current, environment);
        vector_push(&visited, &current->id);
        for(size_t i = 0; i < current->transitions.size; i++)
        {
            NFAStateTransition *trans = vector_element_at(&current->transitions, i);
            if(vector_contains(&visited, &trans->target->id, compare_unsigned_ints) < 0
               && vector_contains(&toVisit, trans->target, nfa_state_compare_based_on_id) < 0)
            {
                vector_push(&toVisit, &trans->target);
            }
        }   
    }
    free(visited.data);
    free(toVisit.data);
}

void nfa_for_each_bf(NFA *nfa, NFAStateAction action, void* environment)
{
    Vector visited = vector_init(sizeof(unsigned int));
    Vector toVisit = vector_init(sizeof(NFAState*));
    vector_push(&toVisit, &nfa->start);
    while(!vector_is_empty(&toVisit))
    {
        NFAState *current;
        vector_poll(&toVisit, &current);
        action(current, environment);
        vector_push(&visited, &current->id);
        for(size_t i = 0; i < current->transitions.size; i++)
        {
            NFAStateTransition *trans = vector_element_at(&current->transitions, i);
            if(vector_contains(&visited, &trans->target->id, compare_unsigned_ints) < 0
               && vector_contains(&toVisit, trans->target, nfa_state_compare_based_on_id) < 0)
            {
                vector_push(&toVisit, &trans->target);
            }
        }   
    }
    free(visited.data);
    free(toVisit.data);
}

void nfa_for_each_sorted(NFA *nfa, NFAStateAction action, void* environment)
{
    Vector visited = vector_init(sizeof(unsigned int));
    Vector toVisit = vector_init(sizeof(NFAState*));
    vector_push(&toVisit, &nfa->start);
    while(!vector_is_empty(&toVisit))
    {
        NFAState *current;
        vector_poll(&toVisit, &current);
        action(current, environment);
        vector_push(&visited, &current->id);
        for(size_t i = 0; i < current->transitions.size; i++)
        {
            NFAStateTransition *trans = vector_element_at(&current->transitions, i);
            if(vector_contains(&visited, &trans->target->id, compare_unsigned_ints) < 0
               && vector_contains(&toVisit, trans->target, nfa_state_compare_based_on_id) < 0)
            {
                vector_insert_sorted(&toVisit, &trans->target, nfa_state_ordering);
            }
        }   
    }
    free(visited.data);
    free(toVisit.data);
}

static char *generate_transition_string(Set *chars)
{
    char *buf;
    size_t len;
    FILE *stream = open_memstream(&buf, &len);
    RBTInorderIterator *it = rbt_inorder_iterator_create(&chars->tree);
    RedBlackTreeNode *current;
    char c;
    while((current = rbt_inorder_iterator_get(it)) != NULL)
    {
        c = *(char*)current->data;
        switch(c)
        {
            // mermaid escapes
            case '|':
            case '(':
            case ')':
            case '[':
            case ']':
            case '{':
            case '}':
            case ';':
            case '\\':
                fprintf(stream, "\"%c\"", c);
                break;
            case '"':
                fprintf(stream, "\"#quot;\"");
                break;
            // control characters
            case '\n':
                fprintf(stream, "\\n");
                break;
            case '\t':
                fprintf(stream, "\\t");
                break;
            // special transition characters
            case ANY_CHARACTER:
                fprintf(stream, "..");
                break;
            case EPSILON_TRANSITION:
                fprintf(stream, "\"#949;\"");
                break;
            default:
                fprintf(stream, "%c", c);
        }
        rbt_inorder_iterator_next(it);
        fprintf(stream, " ");
    }
    rbt_inorder_iterator_free(it);
    fprintf(stream, "%c", 0);
    fclose(stream);
    return buf;
}

static void nfa_state_to_mermaid_string(NFAState *state, void *environment)
{
    struct {
        FILE *stream;
        size_t transNo;
    } *env = environment;
    char *str;
    size_t len;
    for(size_t i = 0; i < state->groupStart.size; i++)
    {
        unsigned int *g = vector_element_at(&state->groupStart, i);
        fprintf(env->stream, "subgraph g%d\n", *g);
    }
    for(size_t i = 0; i < state->groupAccept.size; i++)
    {
        fprintf(env->stream, "end\n");
    }
    for(size_t i = 0; i < state->transitions.size; i++)
    {
        NFAStateTransition *trans = vector_element_at(&state->transitions, i);
        char *transString = generate_transition_string(&trans->characters);
        if(trans->inverted)
        {
            fprintf(env->stream, "%d -->|%s| %d\n", state->id, transString,trans->target->id);
            fprintf(env->stream, "linkStyle %zu color:red;\n", env->transNo);
        }
        else
            fprintf(env->stream, "%d -->|%s| %d\n", state->id, transString,trans->target->id);
        free(transString);
        env->transNo++;
    }
    if(state->start)
    {
        fprintf(env->stream, "style %d fill:#8899FF, stroke-width:3px\n", state->id);
    }
    if(state->accept)
    {
        fprintf(env->stream, "style %d fill:#44DD55, stroke-width:3px\n", state->id);
    }
}

char *nfa_to_mermaid_string(NFA *nfa)
{
    struct {
        FILE *stream;
        size_t transNo;
    } env;
    char *result;
    size_t len;
    env.stream = open_memstream(&result, &len);
    env.transNo = 0;
    fprintf(env.stream, "graph LR\n");
    nfa_for_each_sorted(nfa, nfa_state_to_mermaid_string, &env);
    fclose(env.stream);
    return result;
}

