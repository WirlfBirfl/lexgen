#ifndef LEXGEN_NFA_UTIL_H
#define LEXGEN_NFA_UTIL_H

#include <stdlib.h>
#include <string.h>

#include "nfa.h"

void nfa_print_transition(NFAStateTransition *);

void nfa_print_state(NFAState *);

void nfa_print(NFA *);

#endif
