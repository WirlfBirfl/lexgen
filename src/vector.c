#include "vector.h"
#include <string.h>

Vector vector_init(size_t elementSize)
{
    Vector res;
    res.capacity = 10;
    res.size = 0;
    res.elementSize = elementSize;
    res.data = malloc(res.capacity * res.elementSize);
    memset(res.data, 0, res.capacity);
    return res;
}

void vector_free(Vector *vector)
{
    free(vector->data);
    free(vector);
}

long long vector_push(Vector *vector, void *data)
{
    if(vector->size >= vector->capacity)
    {
        vector->capacity += 5;
        void *newData = realloc(vector->data, vector->capacity * vector->elementSize);
        if(newData == NULL)
            return -1;
        vector->data = newData;
    }
    memcpy(vector->data + vector->size * vector->elementSize, data, vector->elementSize);
    vector->size++;
    return 0;
}

long long vector_push_unique(Vector *vec, void *data, VectorElementComparator comp)
{
    long long i = vector_contains(vec, data, comp);
    if(i < 0)
    {
        long err = vector_push(vec, data);
        if(err >= 0)
            return vec->size - 1;
        return err;
    }
    return (long long)i;
}

void vector_pop(Vector *vector, void* dest)
{
    if(dest != NULL)
    {
        memcpy(dest, vector->data + (vector->size - 1) * vector->elementSize, vector->elementSize);
    }
    vector->size--;
}

char vector_is_empty(Vector *vector)
{
    return vector->size == 0;
}

void* vector_element_at(Vector *vec, size_t index)
{
    if(index >= vec->size)
        return NULL;
    return vec->data + index * vec->elementSize;
}

void* vector_peek(Vector *vec)
{
    return vector_element_at(vec, vec->size-1);
}

long long vector_contains(Vector *vec, void* other, VectorElementComparator comp)
{
    for(size_t i = 0; i < vec->size; i++)
    {
        void* element = vector_element_at(vec, i);
        if(comp(other, element) == 0)
            return i;
    }
    return -1;
}

void vector_remove_element(Vector *vec, void *element, VectorElementComparator comp)
{
    long index = vector_contains(vec, element, comp);
    if(index < 0)
        return;
    if(index != vec->size - 1)
    {
        memmove(vec->data + index * vec->elementSize, vec->data + (index + 1) * vec->elementSize, vec->elementSize * (vec->size - index + 1));
    }
    vec->size--;
}

void vector_poll(Vector *vec, void *dest)
{
    if(dest != NULL)
        memcpy(dest, vec->data, vec->elementSize);
    vec->size--;
    if(vec->size)
        memmove(vec->data, vec->data + vec->elementSize, vec->elementSize * vec->size);
}

void vector_insert_sorted(Vector *vec, void *data, VectorElementComparator comp)
{
    if(vec->size == 0)
        vector_push(vec, data);
    else
    {
        size_t lowerBound = 0;
        size_t upperBound = vec->size;
        size_t index = vec->size / 2;
        while(lowerBound != upperBound)
        {
            char compResult = comp(data, vector_element_at(vec, index));
            if(compResult == 0)
            {
                break;
            }
            else if(compResult < 0)
            {
                upperBound = index;
            }
            else {
                lowerBound = index + 1;
            }
            index = lowerBound + (upperBound - lowerBound) / 2;
        }
        vector_insert(vec, data, index);
    }
}

void vector_insert(Vector *vec, void *data, size_t index)
{
    if(index >= vec->size)
        vector_push(vec, data);
    else
    {
        if(vec->capacity == vec->size)
        {
            vec->capacity += 5;
            void *newData = realloc(vec->data, vec->capacity * vec->elementSize);
            if(newData == NULL)
                return;
            vec->data = newData;
        }
        size_t elementsToMove = vec->size - index;
        memmove(vec->data + (index + 1) * vec->elementSize, vec->data + index * vec->elementSize, vec->elementSize * elementsToMove);
        memcpy(vec->data + index * vec->elementSize, data, vec->elementSize);
        vec->size++;
    }
}

Vector vector_copy(Vector *other)
{
    Vector new;
    new.size = other->size;
    new.capacity = other->capacity;
    new.elementSize = other->elementSize;
    new.data = malloc(new.capacity * new.elementSize);
    memcpy(new.data, other->data, new.size * new.elementSize);
    return new;
}

void vector_remove_element_at(Vector *vec, size_t index, void *buf)
{
    if(index >= vec->size)
        return;
    if(buf != NULL)
    {
        memcpy(buf, vec->data + vec->elementSize * index, vec->elementSize);
    }
    if(vec->size > 1)
    {
        memmove(vec->data + vec->elementSize * index, vec->data + vec->elementSize * (index + 1), vec->elementSize * (vec->size - index + 1));
    }
    vec->size--;
}

void vector_for_each(Vector *vec, VectorElementClosure closure)
{
    for(size_t i = 0; i < vec->size; i++)
    {
        closure.lambda(i, vector_element_at(vec, i), closure.environment);
    }
}