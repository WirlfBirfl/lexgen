#ifndef LEXGEN_SET_H
#define LEXGEN_SET_H

#include <stdlib.h>

#include "red_black_tree.h"

typedef size_t (*HashFunction)(void*);

typedef struct Set {
    RedBlackTree tree;
    size_t size;
} Set;

Set set_init(RedBlackTreeComparator, size_t);

Set* set_create(RedBlackTreeComparator, size_t);

void set_free(Set*);

void set_free_content(Set*);

char set_insert(Set*, void*);

char set_union_with(Set*, Set*);

char set_difference_from(Set*, Set*);

char set_intersection_with(Set*, Set*);

char set_contains(Set*, void*);

char set_is_subset_of(Set*, Set*);

char set_insert_all(Set*, void*, size_t);

char set_is_equal(Set*, Set*);

#endif