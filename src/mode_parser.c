#include "parser.h"

#include <string.h>
#include <stdio.h>

typedef enum ModeLexerState {
    START, B, BE, BEG, BEGI, BEGIN, E, EN, END, ID
} ModeLexerState;

static void push_token(FileParser *fp, size_t tokenStart, Vector *tokens, ModeToken *tmp)
{
    size_t len = fp->pos-tokenStart + 1;
    tmp->value = malloc(len);
    strncpy(tmp->value, fp->input + tokenStart, len - 1);
    tmp->value[len - 1] = '\0';
    vector_push(tokens, tmp);
}

static void handle_switch_intermediate_states(FileParser *fp, char current, char continuation, ModeLexerState next, ModeLexerState *state, ModeToken *tmp, size_t *tokenStart, Vector *tokens)
{
    size_t len;
    if(current == continuation)
    {
        *state = next;
    }
    else if(current == ' ' || current == '\t' || current == ',')
    {
        *state = START;
        push_token(fp, *tokenStart, tokens, tmp);
        return;
    }
    else
    {
        *state = ID;
    }
    fp->pos++;
}

Vector parse_tokenize_modes(FileParser *fp)
{
    Vector tokens = vector_init(sizeof(ModeToken));
    ModeLexerState state = START;
    size_t tokenStart;
    size_t len;
    ModeToken tmp;
    while(fp->input[fp->pos] != ';' && fp->input[fp->pos] != '\n' && fp->input[fp->pos] != EOF)
    {
        char c = fp->input[fp->pos];
        switch(state)
        {
            case START:
                tokenStart = fp->pos;
                if(c == 'b')
                {
                    state = B;
                }
                else if(c == 'e')
                {
                    state = E;
                }
                else if(c == ',')
                {
                    tmp.kind = MODE_COMMA;
                    tmp.value = NULL;
                    vector_push(&tokens, &tmp);
                    fp->pos++;
                    continue;
                }
                else if(c == ' ' || c == '\t')
                {
                    fp->pos++;
                    tokenStart++;
                    continue;
                }
                else
                {
                    state = ID;
                }
                tmp.kind = MODE_ID;
                fp->pos++;
                break;
            case BEGIN:
            case END:
                if(c == ' ' || c == '\t' || c == ',')
                {
                    if(state == END)
                        tmp.kind = MODE_END;
                    else
                        tmp.kind = MODE_BEGIN;
                    state = START;
                    push_token(fp, tokenStart, &tokens, &tmp);
                    continue;
                }
                state = ID;
                fp->pos++;
                break;
            case B:
                handle_switch_intermediate_states(fp, c, 'e', BE, &state, &tmp, &tokenStart, &tokens);
                break;
            case BE:
                handle_switch_intermediate_states(fp, c, 'g', BEG, &state, &tmp, &tokenStart, &tokens);
                break;
            case BEG:
                handle_switch_intermediate_states(fp, c, 'i', BEGI, &state, &tmp, &tokenStart, &tokens);
                break;
            case BEGI:
                handle_switch_intermediate_states(fp, c, 'n', BEGIN, &state, &tmp, &tokenStart, &tokens);
                break;
            case E:
                handle_switch_intermediate_states(fp, c, 'n', EN, &state, &tmp, &tokenStart, &tokens);
                break;
            case EN:
                handle_switch_intermediate_states(fp, c, 'd', END, &state, &tmp, &tokenStart, &tokens);
                break;
            case ID:
                if(c == ' ' || c == '\t' || c == ',')
                {
                    state = START;
                    push_token(fp, tokenStart, &tokens, &tmp);
                    continue;
                }
                fp->pos++;
                break;
        }
    }
    if(state != START)
    {
        push_token(fp, tokenStart, &tokens, &tmp);
    }
    return tokens;
}
static long c_string_comparator(void *other, void *elem)
{
    char * fst = *(char**)other;
    char * scd = *(char**)elem;
    return strcmp(fst, scd);
}

Vector parse_modes(FileParser *fp, NFA *nfa)
{
    // parse the mode switches and stuff
    Vector modeTokens = parse_tokenize_modes(fp);
    Vector modes = vector_init(sizeof(long long));
    char state = 0;
    for(size_t i = 0; i < modeTokens.size; i++)
    {
        ModeToken *tmp = vector_element_at(&modeTokens, i);
        switch(state)
        {
            case 0:
                //
                if(tmp->kind == MODE_BEGIN)
                {
                    state = 1;
                }
                else if(tmp->kind == MODE_END)
                {
                    state = 2;
                }
                else if(tmp->kind == MODE_ID)
                {
                    long long i = vector_push_unique(&fp->modes, &tmp->value, c_string_comparator);
                    vector_push(&modes, &i);
                }
                break;
            case 1:
                if(tmp->kind == MODE_ID)
                {
                    long long i = vector_push_unique(&fp->modes, &tmp->value, c_string_comparator);
                    nfa->final->modeStart = i;
                    state = 0;
                }
                else
                {
                    // TODO: error handling
                }
                break;
            case 2:
                if(tmp->kind == MODE_ID)
                {
                    long long i = vector_push_unique(&fp->modes, &tmp->value, c_string_comparator);
                    nfa->final->modeEnd = i;
                    state = 0;
                }
                else
                {
                    // TODO: error handling
                }
                break;
        }
    }
    long long i = 0;
    if(modes.size == 0)
    {
        vector_push(&modes, &i);
    }
    return modes;
}