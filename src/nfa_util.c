#include "nfa_util.h"
#include "vector.h"

#include <stdio.h>
#include <stdlib.h>


void nfa_print_state(NFAState *state)
{
    printf("State %d\n", state->id);
    printf("  Start: %d\tAccepting:%d\n", state->start, state->accept);
    printf("  Transitions:\n");
    for(int i = 0; i < state->transitions.size; i++)
    {
        NFAStateTransition *trans = (NFAStateTransition*) vector_element_at(&state->transitions, i);
        printf("    ");
        nfa_print_transition(trans);
    }
    printf("\n");
}

void nfa_print_transition(NFAStateTransition *transition)
{
    /*if(transition->character < 0)
    {
        printf("\\e -> %d\n",transition->target->id);
    }
    else
    {
        printf("%c -> %d\n", transition->character, transition->target->id);
    }*/
}

void nfa_print(NFA *nfa)
{
    printf("NFA with %d states:\n", nfa->count);
    printf("--------------\n");
    Vector linearized = nfa_linearize(nfa);
    for(size_t i=0; i<linearized.size; i++)
    {
        NFAState *current = *(NFAState**)vector_element_at(&linearized, i);
        nfa_print_state(current);
    }
    free(linearized.data);
    printf("\n");
}