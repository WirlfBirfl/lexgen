#ifndef LEXGEN_MODE_PARSER_H
#define LEXGEN_MODE_PARSER_H

#include "vector.h"
#include "file_parser.h"

typedef enum ModeTokenKind {
    MODE_ID, MODE_BEGIN, MODE_END, MODE_COMMA
} ModeTokenKind;

typedef struct ModeToken {
    ModeTokenKind kind;
    char *value;
} ModeToken;

Vector parse_tokenize_modes(FileParser*);

Vector parse_modes(FileParser *fp, NFA *nfa);

#endif