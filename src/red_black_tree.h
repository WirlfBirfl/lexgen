#ifndef LEXGEN_RED_BLACK_TREE
#define LEXGEN_RED_BLACK_TREE

#include <stdlib.h>
#include <stdio.h>

#include "vector.h"

#define RED_BLACK_TREE_BLACK 0
#define RED_BLACK_TREE_RED 1

#define RED_BLACK_TREE_NODE_CREATE_FAILURE -1
#define RED_BLACK_TREE_DATA_ALLOC_FAILURE -2

#define RED_BLACK_TREE_VALUE_INSERTED 1
#define RED_BLACK_TREE_VALUE_REMOVED 1

#define RED_BLACK_TREE_RIGHT 0
#define RED_BLACK_TREE_LEFT 1

typedef struct RedBlackTree RedBlackTree;

typedef struct RedBlackTreeNode RedBlackTreeNode;

typedef char (*RedBlackTreeComparator)(void*, void*);

typedef void (*RedBlackTreeNodeAction)(RedBlackTreeNode*, void*);

typedef char* (*RedBlackTreeContentToString)(void *);

typedef struct RBTInorderIterator {
    Vector stack;
    RedBlackTreeNode *current;
} RBTInorderIterator;

typedef struct RedBlackTreeClosure {
    void *environment;
    RedBlackTreeNodeAction action;
} RedBlackTreeClosure;

typedef struct RedBlackTreeNode {
    struct RedBlackTreeNode *left;
    struct RedBlackTreeNode *right;
    struct RedBlackTreeNode *parent;
    void *data;
    char color;
} RedBlackTreeNode;

typedef struct RedBlackTree {
    RedBlackTreeNode *root;
    size_t elementSize;
    RedBlackTreeComparator comparator;
} RedBlackTree;

RedBlackTree* red_black_tree_create(size_t, RedBlackTreeComparator);

void red_black_tree_free(RedBlackTree*);

void red_black_tree_free_data(RedBlackTree*);

RedBlackTreeNode* red_black_tree_node_create();

char red_black_tree_insert(RedBlackTree*, void*);

char red_black_tree_contains(RedBlackTree *, void *);

char red_black_tree_delete(RedBlackTree*, void*);

void red_black_tree_for_each_inorder(RedBlackTree*, RedBlackTreeClosure*);

void red_black_tree_for_each_preorder(RedBlackTree*, RedBlackTreeClosure*);

char *red_black_tree_to_mermaid_string(RedBlackTree*, RedBlackTreeContentToString);

RBTInorderIterator *rbt_inorder_iterator_create(RedBlackTree *);

char rbt_inorder_iterator_has_next(RBTInorderIterator*);

void rbt_inorder_iterator_next(RBTInorderIterator*);

RedBlackTreeNode *rbt_inorder_iterator_get(RBTInorderIterator*);

void rbt_inorder_iterator_free(RBTInorderIterator *);

#endif