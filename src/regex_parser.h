#ifndef LEXGEN_REGEX_PARSER_H
#define LEXGEN_REGEX_PARSER_H

#include "vector.h"
#include "nfa.h"
#include "file_parser.h"

#define ANY_CHARACTER -2

typedef enum RegexTokenKind {
    REGEX_CHAR, REGEX_KLEENE, REGEX_PLUS, REGEX_OPTION, REGEX_CHOICE, REGEX_OPENPAR,
    REGEX_CLOSEPAR, REGEX_END, REGEX_CONCAT, REGEX_WILDCARD, REGEX_OPENBRA, REGEX_CLOSEBRA,
    REGEX_MINUS, REGEX_CARET
} RegexTokenKind;

typedef struct RegexToken {
    RegexTokenKind kind;
    char value;
} RegexToken;

typedef struct RegexParser {
    Vector tokens;
    size_t position;
} RegexParser;


Vector parse_tokenize_regex(FileParser*);

NFA parse_regex(FileParser*);

NFA parse_regex_choice(RegexParser*);

NFA parse_regex_concat(RegexParser*);

NFA parse_regex_primary(RegexParser*);

NFA parse_regex_postfix(RegexParser*);

NFA parse_regex_brackets_content(RegexParser*);

#endif