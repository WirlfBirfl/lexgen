#ifndef LEXGEN_DFA_H
#define LEXGEN_DFA_H

#include "vector.h"
#include "nfa.h"

typedef struct DFAStateTransition {
    Vector characterSet;
    DFAState target;
    char inverted;
} DFAStateTransition;

typedef struct DFAState {
    unsigned int id;
    unsigned int accept;
    unsigned int mode;
    Vector transitions;
} DFAState;

typedef struct DFA {
    DFAState start;
} DFA;

DFA dfa_from_nfa(NFA *);

#endif