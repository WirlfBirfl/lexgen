#ifndef LEXGEN_UTIL_H
#define LEXGEN_UTIL_H

#include <stdlib.h>

typedef enum _either_type_constructor {
    Either_Success, Either_Failure
} EitherConstructor;

typedef struct Either {
    EitherConstructor constr;
    union {
        void *data;
        char *errorMsg;
    } content;
} Either;

#endif