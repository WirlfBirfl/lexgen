#ifndef LEXGEN_VECTOR_H
#define LEXGEN_VECTOR_H

#include <stdlib.h>

#define VECTOR_FOR_I(vec, type, code) \
    do\
    {\
        for(size_t i= 0; i < vec.size; i++)\
        {\
            type *elem = vector_element_at(&vec, i);\
            code;\
        }\
    } while(0);

typedef long (*VectorElementComparator)(void*, void*);

typedef void (*VectorElementClosureFunction)(size_t, void*, void*);

typedef struct VectorElementClosure {
    void *environment;
    VectorElementClosureFunction lambda;
} VectorElementClosure;

typedef struct Vector {
    size_t size;
    size_t capacity;
    size_t elementSize;
    void *data;
} Vector;

Vector vector_init(size_t);

void vector_free(Vector*);

long long vector_push(Vector*, void*);

long long vector_push_unique(Vector*, void*, VectorElementComparator);

void vector_insert_sorted(Vector*, void*, VectorElementComparator);

void vector_insert(Vector*, void*, size_t);

void vector_pop(Vector*, void*);

void* vector_peek(Vector*);

char vector_is_empty(Vector*);

void vector_add(Vector*, void*);

void* vector_element_at(Vector*, size_t);

long long vector_contains(Vector*, void*, VectorElementComparator);

void vector_remove_element(Vector*, void*, VectorElementComparator);

void vector_remove_element_at(Vector*, size_t, void*);

void vector_poll(Vector*, void*);

void vector_for_each(Vector*, VectorElementClosure);

Vector vector_copy(Vector*);

#endif