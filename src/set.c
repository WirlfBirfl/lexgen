#include "set.h"

Set set_init(RedBlackTreeComparator comp, size_t elementSize)
{
    Set set;
    set.size = 0;
    set.tree.root = NULL;
    set.tree.elementSize = elementSize;
    set.tree.comparator = comp;
    return set;
}

Set* set_create(RedBlackTreeComparator comp, size_t elementSize)
{
    Set *set = malloc(sizeof(Set));
    set->size = 0;
    set->tree.root = NULL;
    set->tree.elementSize = elementSize;
    set->tree.comparator = comp;
    return set;
}

void set_free(Set *set)
{
    red_black_tree_free_data(&set->tree);
    free(set);
}

void set_free_content(Set *set)
{
    red_black_tree_free_data(&set->tree);
}

char set_insert(Set *set, void *data)
{
    char res = red_black_tree_insert(&set->tree, data);
    if(res == RED_BLACK_TREE_VALUE_INSERTED)
        set->size++;
    return res;
}

char set_union_with(Set *self, Set *other)
{
    RBTInorderIterator *otherIt = rbt_inorder_iterator_create(&other->tree);
    RedBlackTreeNode *current;
    char res = 0;
    while((current = rbt_inorder_iterator_get(otherIt)) != NULL)
    {
        res = res || set_insert(self, current->data);
    }
    return res;
}

char set_difference_from(Set *self, Set *other)
{
    Set *tmp = set_create(self->tree.comparator, self->tree.elementSize);
    RBTInorderIterator *selfIt = rbt_inorder_iterator_create(&self->tree);
    RBTInorderIterator *otherIt = rbt_inorder_iterator_create(&other->tree);
    RedBlackTreeNode *currentSelf, *currentOther;
    char changed = 0;
    while(1)
    {
        currentSelf = rbt_inorder_iterator_get(selfIt);
        currentOther = rbt_inorder_iterator_get(otherIt);
        if(currentSelf == NULL)
            break;
        if(currentOther == NULL)
        {
            while(currentSelf != NULL)
            {
                set_insert(tmp, currentSelf->data);
                rbt_inorder_iterator_next(selfIt);
                currentSelf = rbt_inorder_iterator_get(selfIt);
            }
            break;
        }
        char comp = self->tree.comparator(currentSelf->data, currentOther->data);
        if(comp == 0)
        {
            rbt_inorder_iterator_next(selfIt);
            rbt_inorder_iterator_next(otherIt);
            changed = 1;
        }
        else if(comp > 0)
        {
            rbt_inorder_iterator_next(otherIt);
        }
        else // if(comp < 0)
        {
            set_insert(tmp, currentSelf->data);
            rbt_inorder_iterator_next(selfIt);
        }
    }
    *self = *tmp;
    free(tmp);
    return changed;
}

char set_intersection_with(Set *self, Set *other)
{
    Set *tmp = set_create(self->tree.comparator, self->tree.elementSize);
    RBTInorderIterator *selfIt = rbt_inorder_iterator_create(&self->tree);
    RBTInorderIterator *otherIt = rbt_inorder_iterator_create(&other->tree);
    RedBlackTreeNode *currentSelf, *currentOther;
    char changed = 0;
    while(1)
    {
        currentSelf = rbt_inorder_iterator_get(selfIt);
        currentOther = rbt_inorder_iterator_get(otherIt);
        if(currentSelf == NULL)
            break;
        if(currentOther == NULL)
        {
            while(currentSelf != NULL)
            {
                set_insert(tmp, currentSelf->data);
                rbt_inorder_iterator_next(selfIt);
                currentSelf = rbt_inorder_iterator_get(selfIt);
            }
            break;
        }
        char comp = self->tree.comparator(currentSelf->data, currentOther->data);
        if(comp == 0)
        {
            rbt_inorder_iterator_next(selfIt);
            rbt_inorder_iterator_next(otherIt);
            set_insert(tmp, currentSelf->data);
            changed = 1;
        }
        else if(comp > 0)
        {
            rbt_inorder_iterator_next(otherIt);
        }
        else // if(comp < 0)
        {
            rbt_inorder_iterator_next(selfIt);
        }
    }
    *self = *tmp;
    free(tmp);
    return changed;

}

char set_contains(Set *set, void *data)
{
    return red_black_tree_contains(&set->tree, data);
}

char set_is_subset_of(Set *sub, Set *super)
{
    if(sub->size > super->size)
        return 0;
    RBTInorderIterator *subIt = rbt_inorder_iterator_create(&sub->tree);
    RBTInorderIterator *superIt = rbt_inorder_iterator_create(&super->tree);
    RedBlackTreeNode *subCurrent, *superCurrent;
    while(1)
    {
        subCurrent = rbt_inorder_iterator_get(subIt);
        superCurrent = rbt_inorder_iterator_get(superIt);
        if(subCurrent == NULL)
            return 1;
        if(superCurrent == NULL)
            return 0;
        char comp = sub->tree.comparator(subCurrent->data, superCurrent->data);
        if(comp == 0)
        {
            rbt_inorder_iterator_next(subIt);
            rbt_inorder_iterator_next(superIt);
        }
        else if(comp > 0)
        {
            rbt_inorder_iterator_next(superIt);
        }
        else // comp < 0
        {
            return 0;
        }
    }
}

char set_insert_all(Set *set, void *dataStart, size_t len)
{
    char changed = 0;
    for(size_t i = 0; i < len; i++)
    {
        changed = set_insert(set, dataStart) || changed;
        dataStart += set->tree.elementSize;
    }
    return changed;
}

char set_is_equal(Set *self, Set* other)
{
    if(self->size != other->size)
        return 0;
    RBTInorderIterator *selfIt = rbt_inorder_iterator_create(&self->tree);
    RBTInorderIterator *otherIt = rbt_inorder_iterator_create(&other->tree);
    RedBlackTreeNode *selfCurrent, *otherCurrent;
    while(1)
    {
        selfCurrent = rbt_inorder_iterator_get(selfIt);
        otherCurrent = rbt_inorder_iterator_get(otherIt);
        if(selfCurrent == NULL)
            return 1;
        char comp = self->tree.comparator(selfCurrent->data, otherCurrent->data);
        if(comp == 0)
        {
            rbt_inorder_iterator_next(selfIt);
            rbt_inorder_iterator_next(otherIt);
        }
        else
        {
            return 0;
        }
    }
    
}
