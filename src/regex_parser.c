#include "parser.h"
#include "string.h"
#include "nfa_util.h"
#include <stdio.h>

static void parse_handle_escape_chars(char c, Vector *tokens)
{
    RegexToken tmp;
    switch(c)
    {
        case '|':
        case '(':
        case ')':
        case '?':
        case '*':
        case '+':
        case '.':
        case '[':
        case ']':
        case '-':
        case '\\':
        case ';':
        case '^':
            tmp.kind = REGEX_CHAR;
            tmp.value = c;
            vector_push(tokens, &tmp);
            break;
        case 's':
            tmp.kind = REGEX_OPENBRA;
            tmp.value = '[';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '\n';
            vector_push(tokens, &tmp);
            tmp.kind = REGEX_CHAR;
            tmp.value = '\t';
            vector_push(tokens, &tmp);
            tmp.kind = REGEX_CHAR;
            tmp.value = ' ';
            vector_push(tokens, &tmp);

            tmp.kind = REGEX_CLOSEBRA;
            tmp.value = ']';
            vector_push(tokens, &tmp);
            break;
        case 'd':
            tmp.kind = REGEX_OPENBRA;
            tmp.value = '[';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '0';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '1';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '2';
            vector_push(tokens, &tmp);

            tmp.kind = REGEX_CHAR;
            tmp.value = '3';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '4';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '5';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '6';
            vector_push(tokens, &tmp);

            tmp.kind = REGEX_CHAR;
            tmp.value = '7';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '8';
            vector_push(tokens, &tmp);
            
            tmp.kind = REGEX_CHAR;
            tmp.value = '9';
            vector_push(tokens, &tmp);

            tmp.kind = REGEX_CLOSEBRA;
            tmp.value = ']';
            vector_push(tokens, &tmp);
            break;
        case 'n':
            tmp.kind = REGEX_CHAR;
            tmp.value = '\n';
            vector_push(tokens, &tmp);
            break;
        case 't':
            tmp.kind = REGEX_CHAR;
            tmp.value = '\t';
            vector_push(tokens, &tmp);
        default:
            break;
    }
}

static void remove_doubled_parenthesis(Vector *tokens)
{
    long openParStart = -1;
    size_t openedParsCount = 0;
    RegexToken *tmp = NULL;
    for(size_t i = 1; i < tokens->size - 1;)
    {
        tmp = vector_element_at(tokens, i);
        if(openParStart == -1)
        {
            if(tmp->kind != REGEX_OPENPAR)
            {
                i += 2;
                continue;
            }
            tmp = vector_element_at(tokens, i - 1);
            i++;
            if(tmp->kind != REGEX_OPENPAR)
            {
                continue;
            }
            openParStart = i - 1;
            openedParsCount = 0;
        }
        else
        {
            if(tmp->kind == REGEX_OPENPAR)
            {
                openedParsCount++;
                i++;
                continue;
            }
            else if(tmp->kind == REGEX_CLOSEPAR)
            {
                if(openedParsCount > 0)
                {
                    openedParsCount--;
                    i++;
                    continue;
                }
                else
                {
                    tmp = vector_element_at(tokens, i + 1);
                    if(tmp->kind != REGEX_CLOSEPAR)
                    {
                        openParStart = -1;
                        openedParsCount = 0;
                        i += 2;
                        continue;
                    }
                    else
                    {
                        vector_remove_element_at(tokens, i + 1, NULL);
                        vector_remove_element_at(tokens, openParStart, NULL);
                        i = openParStart;
                        openedParsCount = 0;
                        openParStart = -1;
                        continue;
                    }
                }
            }
            i++;
        }
    }
}

static Vector regex_tokens_compress_postfix_operators(Vector *tokens)
{
    Vector result = vector_init(sizeof(RegexToken));
    size_t opChainSize = 0;
    RegexTokenKind replacement = -1;
    vector_push(&result, vector_element_at(tokens, 0));
    for(size_t i = 1; i < tokens->size; i++)
    {
        RegexToken *fst = vector_element_at(tokens, i);
        if(fst->kind == REGEX_KLEENE || fst->kind == REGEX_PLUS || fst->kind == REGEX_OPTION)
        {
            if(opChainSize == 0)
            {
                replacement = fst->kind;
            }
            else if(replacement != REGEX_KLEENE)
            {
                if(fst->kind == REGEX_KLEENE)
                {
                    replacement = REGEX_KLEENE;
                }
                else if(fst->kind != replacement)
                {
                    replacement = REGEX_KLEENE;
                }
            }
            opChainSize++;
        }
        else 
        {
            if(opChainSize > 1)
            {
                RegexToken tmp;
                tmp.kind = replacement;
                tmp.value = '\0';
                vector_push(&result, &tmp);
            }
            else if(opChainSize == 1)
            {
                vector_push(&result, vector_element_at(tokens, i-1));
            }
            replacement = -1;
            opChainSize = 0;
            vector_push(&result, fst);
        }

    }
    return result;
} 

Vector parse_tokenize_regex(FileParser *fp)
{
    Vector tokens = vector_init(sizeof(RegexToken));
    RegexToken tmp;
    char escape = 0;
    while(!(fp->input[fp->pos] == ';' && escape == 0) && (fp->input[fp->pos] != '\n'))
    {
        if(escape)
        {
            escape = 2;
            parse_handle_escape_chars(fp->input[fp->pos], &tokens);
        }
        else
        switch(fp->input[fp->pos])
        {
            case '|':
                tmp.kind = REGEX_CHOICE;
                break;
            case '(':
                tmp.kind = REGEX_OPENPAR;
                break;
            case ')':
                tmp.kind = REGEX_CLOSEPAR;
                break;
            case '?':
                tmp.kind = REGEX_OPTION;
                break;
            case '*':
                tmp.kind = REGEX_KLEENE;
                break;
            case '+':
                tmp.kind = REGEX_PLUS;
                break;
            case '.':
                tmp.kind = REGEX_WILDCARD;
                break;
            case '[':
                tmp.kind = REGEX_OPENBRA;
                break;
            case ']':
                tmp.kind = REGEX_CLOSEBRA;
                break;
            case '-':
                tmp.kind = REGEX_MINUS;
                break;
            case '^':
                tmp.kind = REGEX_CARET;
                break;
            case '\\':
                escape = 1;
                break;
            case '\t':
                fp->pos++;
                continue;
            default:
                tmp.kind = REGEX_CHAR;
                break;
        }
        if(!escape)
        {
            tmp.value = fp->input[fp->pos];
            vector_push(&tokens, &tmp);
        }
        fp->pos++;
        escape = (escape == 2) ? 0 : escape;
    }
    // TODO: check if there is a dangling backslash (i.e., if we went into escape mode, but then a newline or ; came next)
    tmp.kind = REGEX_END;
    tmp.value = 0;
    vector_push(&tokens, &tmp);
    remove_doubled_parenthesis(&tokens);
    Vector operatorCompressed = regex_tokens_compress_postfix_operators(&tokens);
    free(tokens.data);
    Vector finalResult = vector_init(sizeof(RegexToken));
    char state = 0;
    // states: 
    // 0 -> just starting or started a bracketed part, or a new choice
    // 1 -> last thing consumed was a char, a wildcard or closed a bracketed part
    // 2 -> last thing consumed was nothing above
    // 3 -> within brackets, no concat at all
    RegexToken concat;
    concat.kind = REGEX_CONCAT;
    concat.value = -1;
    for(size_t i = 0; i < operatorCompressed.size; i++)
    {
        RegexToken *tmp = vector_element_at(&operatorCompressed, i);
        switch(state)
        {
            case 0:
                if(tmp->kind == REGEX_CHAR || tmp->kind == REGEX_WILDCARD)
                {
                    vector_push(&finalResult, tmp);
                    state = 1;
                }
                else if(tmp->kind == REGEX_OPENPAR || tmp->kind == REGEX_CHOICE || tmp->kind == REGEX_CARET)
                {
                    vector_push(&finalResult, tmp);
                }
                else if(tmp->kind == REGEX_CLOSEPAR || tmp->kind == REGEX_CLOSEBRA) 
                {
                    vector_push(&finalResult, tmp);
                    state = 1;
                }
                else if(tmp->kind == REGEX_OPENBRA)
                {
                    vector_push(&finalResult, tmp);
                    state = 3;
                }
                else
                {
                    vector_push(&finalResult, tmp);
                    state = 2;
                }
                break;
            case 1:
                if(tmp->kind == REGEX_CHAR || tmp->kind == REGEX_WILDCARD)
                {
                    vector_push(&finalResult, &concat);
                    vector_push(&finalResult, tmp);
                }
                else if(tmp->kind == REGEX_OPENPAR || tmp->kind == REGEX_CHOICE || tmp->kind == REGEX_OPENBRA || tmp->kind == REGEX_CARET)
                {
                    if(!(tmp->kind == REGEX_CHOICE))
                        vector_push(&finalResult, &concat);
                    vector_push(&finalResult, tmp);
                    if(tmp->kind == REGEX_OPENBRA)
                        state = 3;
                    else
                        state = 0;
                }
                else if(tmp->kind == REGEX_CLOSEPAR || tmp->kind == REGEX_CLOSEBRA) 
                {
                    vector_push(&finalResult, tmp);
                }
                else
                {
                    vector_push(&finalResult, tmp);
                    state = 2;
                }
                break;
            case 2:
                if(tmp->kind == REGEX_CHAR || tmp->kind == REGEX_WILDCARD)
                {
                    vector_push(&finalResult, &concat);
                    vector_push(&finalResult, tmp);
                    state = 1;
                }
                else if(tmp->kind == REGEX_OPENPAR || tmp->kind == REGEX_CHOICE || tmp->kind == REGEX_OPENBRA || tmp->kind == REGEX_CARET)
                {
                    if(!(tmp->kind == REGEX_CHOICE))
                        vector_push(&finalResult, &concat);
                    vector_push(&finalResult, tmp);
                    if(tmp->kind == REGEX_OPENBRA)
                        state = 3;
                    else
                        state = 0;
                }
                else if(tmp->kind == REGEX_CLOSEPAR || tmp->kind == REGEX_CLOSEBRA) 
                {
                    vector_push(&finalResult, tmp);
                    state = 1;
                }
                else
                {
                    vector_push(&finalResult, tmp);
                }
                break;
            case 3:
                if(tmp->kind == REGEX_CLOSEBRA)
                {
                    state = 1;
                }
                else if(tmp->kind != REGEX_MINUS && tmp->kind != REGEX_CHAR && tmp->kind != REGEX_CARET)
                {
                    tmp->kind = REGEX_CHAR;
                }
                vector_push(&finalResult, tmp);
                break;
            default:
                break;
        }
    }
    free(operatorCompressed.data);
    return finalResult;
}

RegexToken* peek(RegexParser *parser)
{
    return (RegexToken*) vector_element_at(&parser->tokens, parser->position);
}

RegexToken* peek_ahead(RegexParser *parser)
{
    return (RegexToken*) vector_element_at(&parser->tokens, parser->position + 1);
}

void consume(RegexParser *parser)
{
    parser->position++;
}

NFA parse_regex(FileParser *fp)
{
    Vector tokens = parse_tokenize_regex(fp);
    RegexParser parser;
    parser.tokens = tokens;
    parser.position = 0;
    return parse_regex_choice(&parser);
}

NFA parse_regex_choice(RegexParser *parser)
{
    NFA fst = parse_regex_concat(parser);
    RegexToken *current = peek(parser);
    while(current->kind == REGEX_CHOICE)
    {
        consume(parser);
        NFA scd = parse_regex_concat(parser);
        NFA result = nfa_create_choice(&fst, &scd);
        fst = result;
        current = peek(parser);
    }
    return fst;
}

NFA parse_regex_concat(RegexParser *parser)
{
    NFA fst = parse_regex_postfix(parser);
    RegexToken *current = peek(parser);
    while(current->kind == REGEX_CONCAT)
    {
        consume(parser);
        NFA scd = parse_regex_postfix(parser);
        NFA result = nfa_create_concat(&fst, &scd);
        fst = result;
        current = peek(parser);
    }
    return fst;
}

NFA parse_regex_postfix(RegexParser *parser)
{
    NFA primary = parse_regex_primary(parser);
    RegexToken *current = peek(parser);
    NFA result;
    while(1)
    {
        if(current->kind == REGEX_KLEENE)
        {
            primary = nfa_create_kleene(&primary);
        }
        else if(current->kind == REGEX_PLUS)
        {
            primary = nfa_create_oneormore(&primary);
        }
        else if(current->kind == REGEX_OPTION)
        {
            primary = nfa_create_option(&primary);
        }
        else
        {
            break;
        }
        consume(parser);
        current = peek(parser);
    }
    return primary;
}

NFA parse_regex_primary(RegexParser *parser)
{
    NFA dummy;
    RegexToken* current = peek(parser);
    if(current->kind == REGEX_OPENPAR)
    {
        consume(parser);
        NFA result = parse_regex_choice(parser);
        current = peek(parser);
        if(current->kind == REGEX_CLOSEPAR)
        {
            consume(parser);
        }
        else
        {
            // TODO : error handling
        }
        vector_push(&result.start->groupStart, &result.groupCount);
        vector_push(&result.final->groupAccept, &result.groupCount);
        result.groupCount++;
        return result;
    }
    else if(current->kind == REGEX_CHAR)
    {
        NFA nfa = nfa_create_char(current->value);
        consume(parser);
        return nfa;
    }
    else if(current->kind == REGEX_OPENBRA)
    {
        consume(parser);
        NFA result = parse_regex_brackets_content(parser);
        current = peek(parser);
        if(current->kind != REGEX_CLOSEBRA)
        {
            // TODO: error handling
        }
        consume(parser);
        return result;
    }
    else if(current->kind == REGEX_WILDCARD)
    {
        consume(parser);
        NFA result = nfa_create_char(ANY_CHARACTER);
        return result;
    }
    else if(current->kind == REGEX_CARET)
    {
        consume(parser);
        current = peek(parser);
        if(current->kind != REGEX_CHAR)
        {
            // TODO: error handling
        }
        NFA nfa = nfa_create_char_inverted(current->value);
        consume(parser);
        return nfa;
    }
    else
    {
        // TODO : error handling
    }
    return dummy;
}

NFA parse_regex_brackets_content(RegexParser *parser)
{
    RegexToken *current = peek(parser);
    Vector tokens = vector_init(sizeof(char));
    char minusConsumed = 0;
    char inverted = 0;
    if(current->kind == REGEX_CARET)
    {
        inverted = 1;
        consume(parser);
        current = peek(parser);
    }
    while(current->kind != REGEX_CLOSEBRA && current->kind != REGEX_END)
    {
        if(current->kind == REGEX_CHAR)
        {
            if(!minusConsumed)
            {
                // character at start of brackets or just after another character
                vector_push(&tokens, &current->value);
                consume(parser);
            }
            else if(minusConsumed)
            {
                // completed range
                minusConsumed = 0;
                char low;
                vector_pop(&tokens, &low);
                char high = current->value;
                if(low > high)
                {
                    char tmp = low;
                    low = high;
                    high = tmp;
                }
                for(char c = low; c <= high; c++)
                {
                    vector_push(&tokens, &c);
                }
                consume(parser);
            }
        }
        else if(current->kind == REGEX_MINUS)
        {
            if(minusConsumed)
            {
                // TODO: error handling
            }
            else
            {
                // part of maybe valid range expression
                consume(parser);
                minusConsumed = 1;
            }
        }
        else
        {
            // TODO: Error handling
        }
        current = peek(parser);
    }
    if(inverted)
        return nfa_create_from_chars_inverted(tokens.data, tokens.size);
    else
        return nfa_create_from_chars(tokens.data, tokens.size);
}