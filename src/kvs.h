#ifndef LEXGEN_KVS_H
#define LEXGEN_KVS_H

#include <stdlib.h>

#define KVS_INSERT_SUCCESS 0
#define KVS_INSERT_REALLOC_FAILED -1
#define KVS_KEY_TOO_BIG -2

typedef size_t (*HashFunction)(void*);

typedef struct KeyValueStore {
    size_t size;
    size_t elementSize;
    size_t capacity;
    void *data;
    HashFunction hashFunc;
} KeyValueStore;

KeyValueStore kvs_init(HashFunction, size_t);

KeyValueStore kvs_init_cap(HashFunction, size_t, size_t);

void kvs_free(KeyValueStore*);

char kvs_insert(KeyValueStore*, void*, void*);

void *kvs_get(KeyValueStore*, void*);

#endif