#include "parser.h"
#include "nfa.h"

#include <stdlib.h>
#include <stdio.h>

char *__MODE__DEFAULT = "default";

FileParseResult parse_file(char *file)
{
    FileParser fp;
    fp.input = file;
    fp.pos = 0;
    fp.ruleNo = 1;
    fp.modes = vector_init(sizeof(char*));
    vector_push(&fp.modes, &__MODE__DEFAULT);
    Vector lines = parse_lines(&fp);
    if(fp.input[fp.pos] != EOF)
    {
        // TODO: some error happened
    }
    FileParseResult result;
    result.input = file;
    result.modes = fp.modes;
    result.lines = lines;
    result.errors = vector_init(sizeof(FileParseError));
    return result;
}

Vector parse_lines(FileParser *fp)
{
    Vector lines = vector_init(sizeof(Line));
    while(fp->input[fp->pos] == '\n')
    {
        fp->pos++;
    }
    while(fp->input[fp->pos] != EOF)
    {
        Line l = parse_line(fp);
        vector_push(&lines, &l);
        fp->ruleNo++;
        while(fp->input[fp->pos] == '\n')
        {
            fp->pos++;
        }
    }
    return lines;
}


Line parse_line(FileParser *fp)
{
    Line line;
    line.nfa = parse_regex(fp);
    line.codeSlice.index = 0;
    line.codeSlice.len = 0;
    line.nfa.final->accept = fp->ruleNo;
    if(fp->input[fp->pos] == ';')
    {
        fp->pos++;
    }
    else if(fp->input[fp->pos] == '\n')
    {
        line.modes = vector_init(sizeof(long long));
        long long i = 0;
        vector_push(&line.modes, &i);
        return line;
    }
    else
    {
        // TODO: some error
    }
    line.modes = parse_modes(fp, &line.nfa);
    // TODO: parse rest of line
    if(fp->input[fp->pos] == ';')
    {
        fp->pos++;
    }
    else if(fp->input[fp->pos] == '\n')
    {
        return line;
    }
    else
    {
        // TODO: some error
    }
    // TODO: parse C code
    // not actually parsing the code but check that its properly fitting into the format
    return line;
}
