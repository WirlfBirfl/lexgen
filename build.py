import glob
import subprocess
import sys

if __name__ == "__main__":
        
    cfiles = glob.glob("src/*.c")

    compilerCmd = ["clang", "-g", "-o", "bin/lexgen", *cfiles]

    if len(sys.argv) == 2:
        testfiles = glob.glob("test/*.c")
        cfiles = [f for f in cfiles if f != "src/main.c"]
        testCompilerCmd = ["clang", "-g","-Isrc","-o", "bin/lexgen_tests", *cfiles, *testfiles]
        process = subprocess.Popen(testCompilerCmd)
        result = process.wait()
        runTests = ["./bin/lexgen_tests"]
        process = subprocess.Popen(runTests)
        result = process.wait()
        if result != 0:
            exit()
    else:
        process = subprocess.Popen(compilerCmd)
        process.wait()