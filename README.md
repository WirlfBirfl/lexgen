# A simple lexer generator written in C
This is a pet project, just to see if I can do it.
The general idea is to make a simple flex clone in C.
It is also meant to demonstrate and explain to others,
how it works.
## Feautures
* Generate a lexer, first and foremost.
* Make the internal process visible
## Build
I'm building on linux with clang. There is a python 3.x build script in the root of the repo.
## Input format
The input is given in lines. One line contains a regular expression, some information about modes and changes, and C code. C code may span multiple lines. Everything is separated via semicolons. SO the general format is this:
```
REGEX ; MODEINFO ; C_CODE
```
### Regex
#### Operators
The regex can have the usual operators:
* Kleene star `*`
* One ore more `+`
* Option `?`
* Choice `|`
* Anything but `^`
#### Ranges
They also support the range syntax `[0-9]` or `[0-9a-f]`. Inverted or 'anything but' ranges are also possible: `[^0-9]` matches anything but a number from 0 to 9.
#### Groups
Every expression in parenthesis is a group, e.g. in the expression `a(b|c)`, `(b|c)` is a group and can be referenced with its group number, which starts at 0 with the dollar syntax, `$0`. A group can also be captured multiple times, so it is possible to index every occurrence.
#### Wildcard
The `.` matches any character.
## Internal workings
Each line containing a regex is converted to a NFA accepting that regex. Then those NFAs are combined into one NFA, This NFA is then converted to a DFA and then minimized. Then code will be generated from that DFA.
### NFA
NFAs for regexes are generated from one basic atom NFA, that is accepting a single character. All those atom NFAs are then combined, by creating combination of those NFAs according to the operators used.
#### NFA from char 
Regex:
```
a;
```
NFA:
```mermaid
graph LR
0 -->|a| 1
style 0 fill:#8899FF, stroke-width:3px
style 1 fill:#44DD55, stroke-width:3px
```
Note: Below, the NFA is shown for an operator combined from two sub expressions. Those subexpressions are highlighted as groups, which can be achieved by putting them into parenthesis. So the displayed NFA does not exactly correspond to the regex.
### NFA concatination
Regex
```
ab;
```
NFA:
```mermaid
graph LR
subgraph g0
0 -->|a| 1
style 0 fill:#8899FF, stroke-width:3px
end
1 -->|"#949;"| 2
subgraph g1
2 -->|b| 3
end
style 3 fill:#44DD55, stroke-width:3px
```
### NFA choice
Regex
```
a|b;
```
NFA:
```mermaid
graph LR
0 -->|"#949;"| 1
0 -->|"#949;"| 3
style 0 fill:#8899FF, stroke-width:3px
subgraph g0
1 -->|a| 2
end
2 -->|"#949;"| 5
subgraph g1
3 -->|b| 4
end
4 -->|"#949;"| 5
style 5 fill:#44DD55, stroke-width:3px
```
### NFA option
Regex
```
a?;
```
NFA:
```mermaid
graph LR
0 -->|"#949;"| 1
0 -->|"#949;"| 3
style 0 fill:#8899FF, stroke-width:3px
subgraph g0
1 -->|a| 2
end
2 -->|"#949;"| 3
style 3 fill:#44DD55, stroke-width:3px
```
### NFA kleene star
Regex
```
a*;
```
NFA:
```mermaid
graph LR
0 -->|"#949;"| 1
0 -->|"#949;"| 3
style 0 fill:#8899FF, stroke-width:3px
subgraph g0
1 -->|a| 2
end
2 -->|"#949;"| 3
3 -->|"#949;"| 0
style 3 fill:#44DD55, stroke-width:3px
```
### NFA plus
Regex
```
a+;
```
NFA:
```mermaid
graph LR
0 -->|"#949;"| 1
style 0 fill:#8899FF, stroke-width:3px
subgraph g0
1 -->|a| 2
end
2 -->|"#949;"| 3
3 -->|"#949;"| 0
style 3 fill:#44DD55, stroke-width:3px
```
