#include "lexgen_tests.h"
#include "minunit.h"
#include "vector.h"

#include <stdio.h>

static char long_comp(long *fst, long *scd)
{
    return (char)(*fst - *scd);
}

static char *test_vector_insert_sorted()
{
    Vector v = vector_init(sizeof(long));
    long data[] = {1, 4, 5, 9, 0, 2, 3};
    long dataSorted[] = {0, 1, 2, 3, 4, 5, 9};
    vector_insert_sorted(&v, data, (VectorElementComparator)long_comp);
    vector_insert_sorted(&v, data + 1, (VectorElementComparator)long_comp);
    vector_insert_sorted(&v, data + 2, (VectorElementComparator)long_comp);
    vector_insert_sorted(&v, data + 3, (VectorElementComparator)long_comp);
    vector_insert_sorted(&v, data + 4, (VectorElementComparator)long_comp);
    vector_insert_sorted(&v, data + 5, (VectorElementComparator)long_comp);
    vector_insert_sorted(&v, data + 6, (VectorElementComparator)long_comp);
    for(int i = 0; i < v.size; i++)
    {
        mu_assert("not sorted correctly", *(long*)vector_element_at(&v, i) == dataSorted[i]);
    }
    free(v.data);
    return 0;
}

static char *test_vector_poll()
{
    Vector v = vector_init(sizeof(long));
    long data[] = {1, 4, 5, 9, 0, 2, 3};
    vector_push(&v, data);
    vector_push(&v, data + 1);
    vector_push(&v, data + 2);
    vector_push(&v, data + 3);
    vector_push(&v, data + 4);
    vector_push(&v, data + 5);
    vector_push(&v, data + 6);
    long polled;
    vector_poll(&v, &polled);
    mu_assert("first polled is not first pushed", polled == 1);
    free(v.data);
    return 0;
}

static char *test_vector_element_at()
{
    Vector v = vector_init(sizeof(int));
    int tests[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    vector_push(&v, tests);
    vector_push(&v, tests + 1);
    vector_push(&v, tests + 2);
    vector_push(&v, tests + 3);
    vector_push(&v, tests + 4);
    vector_push(&v, tests + 5);
    vector_push(&v, tests + 6);
    vector_push(&v, tests + 7);
    vector_push(&v, tests + 8);
    vector_push(&v, tests + 9);
    int *tmp;
    tmp = (int*)vector_element_at(&v, 0);
    mu_assert("element 0 is not 1", *tmp == 1);
    tmp = (int*)vector_element_at(&v, 1);
    mu_assert("element 1 is not 2", *tmp == 2);
    tmp = (int*)vector_element_at(&v, 2);
    mu_assert("element 2 is not 3", *tmp == 3);
    tmp = (int*)vector_element_at(&v, 3);
    mu_assert("element 3 is not 4", *tmp == 4);
    tmp = (int*)vector_element_at(&v, 4);
    mu_assert("element 4 is not 5", *tmp == 5);
    tmp = (int*)vector_element_at(&v, 5);
    mu_assert("element 5 is not 6", *tmp == 6);
    tmp = (int*)vector_element_at(&v, 6);
    mu_assert("element 6 is not 7", *tmp == 7);
    tmp = (int*)vector_element_at(&v, 7);
    mu_assert("element 7 is not 8", *tmp == 8);
    tmp = (int*)vector_element_at(&v, 8);
    mu_assert("element 8 is not 9", *tmp == 9);
    tmp = (int*)vector_element_at(&v, 9);
    mu_assert("element 9 is not 10", *tmp == 10);
    free(v.data);
    return 0;
}

char *all_vector_tests()
{
    show_testheader("vector");
    mu_run_test("testing element at", test_vector_element_at);
    mu_run_test("testing polling", test_vector_poll);
    mu_run_test("testing insert sorted", test_vector_insert_sorted);
    printf("\n");
    return 0;
}