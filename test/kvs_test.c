#include <stdio.h>

#include "minunit.h"
#include "kvs.h"

static size_t hash_long(void *l)
{
    return *(size_t*)l;
}

static char *test_kvs_insert()
{
    KeyValueStore kvs = kvs_init(hash_long, sizeof(long));
    long val = 134;
    kvs_insert(&kvs, &val, &val);
    void *location = kvs.data + 4 * (kvs.elementSize + sizeof(size_t));
    location += sizeof(size_t);
    long res = *(long *)location;
    mu_assert("value is not 134", res == val);
    
    val = 144;
    kvs_insert(&kvs, &val, &val);
    location = kvs.data + 5 * (kvs.elementSize + sizeof(size_t));
    location += sizeof(size_t);
    res = *(long *)location;
    mu_assert("value is not 144", res == val);
    return 0;
}

static char *test_kvs_get_with_resizing()
{
    KeyValueStore kvs = kvs_init_cap(hash_long, sizeof(long), 1);
    long val = 134;
    kvs_insert(&kvs, &val, &val);
    long res = *(long*)kvs_get(&kvs, &val);
    mu_assert("value is not 134", res == val);
    val = 144;
    kvs_insert(&kvs, &val, &val);
    res = *(long*)kvs_get(&kvs, &val);
    mu_assert("value is not 144", res == val);
    val = 222;
    kvs_insert(&kvs, &val, &val);
    res = *(long*)kvs_get(&kvs, &val);
    mu_assert("value is not 222", res == val);
    val = 139;
    kvs_insert(&kvs, &val, &val);
    res = *(long*)kvs_get(&kvs, &val);
    mu_assert("value is not 139", res == val);
    return 0;
}

static char *test_kvs_get()
{
    KeyValueStore kvs = kvs_init(hash_long, sizeof(long));
    long val = 134;
    kvs_insert(&kvs, &val, &val);
    long res = *(long*)kvs_get(&kvs, &val);
    mu_assert("value is not 134", res == 134);
    val = 144;
    kvs_insert(&kvs, &val, &val);
    res = *(long*)kvs_get(&kvs, &val);
    mu_assert("value is not 144", res == 144);
    return 0;
}


char *all_kvs_tests()
{
    show_testheader("key value store");
    mu_run_test("testing insert", test_kvs_insert);
    mu_run_test("testing get", test_kvs_get);
    mu_run_test("testing get with resizing", test_kvs_get_with_resizing);
    printf("\n");
    return 0;
}