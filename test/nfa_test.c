#include "minunit.h"
#include "nfa.h"
#include "lexgen_tests.h"
#include "vector.h"
#include <stdio.h>

static char* test_nfa_create_from_char()
{
    NFA a = nfa_create_char('d');
    mu_assert("not two states", a.count == 2);
    mu_assert("start state has not id 0", a.start->id == 0);
    mu_assert("start state is not starting", a.start->start > 0);
    mu_assert("start state has not exactly 1 transition", a.start->transitions.size == 1);
    mu_assert("final state has not id 1", a.final->id == 1);
    mu_assert("final state is not accepting", a.final->accept > 0);
    NFAStateTransition *trans = (NFAStateTransition*) vector_element_at(&a.start->transitions, 0);
    RBTInorderIterator *it = rbt_inorder_iterator_create(&trans->characters.tree);
    char fstChar = *(char*)rbt_inorder_iterator_get(it)->data;
    mu_assert("transition character is not d", fstChar == 'd');
    mu_assert("start state is not pointing to final state", trans->target == a.final);
    rbt_inorder_iterator_free(it);
    return 0;
}

static char* test_nfa_create_concat()
{
    NFA fst = nfa_create_char('a');
    NFA scd = nfa_create_char('b');
    NFA concat = nfa_create_concat(&fst, &scd);
    mu_assert("concatinated nfa has not 4 states", concat.count == 4);
    // result start state asserts
    mu_assert("start state id is not 0", concat.start->id == 0);
    mu_assert("start state has not exactly 1 transition", concat.start->transitions.size == 1);
    NFAStateTransition *tmp = vector_element_at(&concat.start->transitions, 0);
    RBTInorderIterator *it = rbt_inorder_iterator_create(&tmp->characters.tree);
    char fstChar = *(char*)rbt_inorder_iterator_get(it)->data;
    mu_assert("start state is not transitioning on letter a", fstChar == 'a');
    mu_assert("start state is not pointing to final state of fst nfa", tmp->target == fst.final);
    rbt_inorder_iterator_free(it);
    // fst final state asserts
    mu_assert("fst final state id is not 1", fst.final->id == 1);
    mu_assert("fst final state has not exactly 1 transition", fst.final->transitions.size == 1);
    tmp = vector_element_at(&fst.final->transitions, 0);
    it = rbt_inorder_iterator_create(&tmp->characters.tree);
    fstChar = *(char*)rbt_inorder_iterator_get(it)->data;
    mu_assert("fst final state is not transitioning on epsilon", fstChar < 0);
    mu_assert("fst final state is not pointing to start state of scd nfa", tmp->target == scd.start);
    rbt_inorder_iterator_free(it);
    // scd start state asserts
    mu_assert("scd start state id is not 2", scd.start->id == 2);
    mu_assert("scd start state has not exactly 1 transition", scd.start->transitions.size == 1);
    tmp = vector_element_at(&scd.start->transitions, 0);
    it = rbt_inorder_iterator_create(&tmp->characters.tree);
    fstChar = *(char*)rbt_inorder_iterator_get(it)->data;
    mu_assert("scd start state is not transitioning letter b", fstChar == 'b');
    mu_assert("scd start state is not pointing to start state of scd nfa", tmp->target == scd.final);
    rbt_inorder_iterator_free(it);
    // result final state asserts
    mu_assert("final state id is not 3", concat.final->id == 3);
    return 0;
}

static char* test_nfa_linearize()
{
    NFA test = nfa_create_char('a');
    Vector v = nfa_linearize(&test);
    mu_assert("vector size is not 2", v.size == 2);
    mu_assert("state 0 in vector is not start state of nfa", test.start == *(NFAState**)vector_element_at(&v, 0));
    mu_assert("state 1 in vector is not final state of nfa", test.final == *(NFAState**)vector_element_at(&v, 1));
    free(v.data);
    return 0;
}

char* all_nfa_tests()
{
    show_testheader("NFA");
    mu_run_test("testing nfa linearization", test_nfa_linearize);
    mu_run_test("testing nfa creation from a character (d)", test_nfa_create_from_char);
    mu_run_test("testing nfa creation from two character NFAs (a) and ((b)", test_nfa_create_concat);
    printf("\n");
    return 0;
}