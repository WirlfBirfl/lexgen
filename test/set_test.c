#include "minunit.h"
#include "red_black_tree.h"
#include "vector.h"
#include "set.h"
#include <stdio.h>

static char compare_ints(void *i1, void *i2)
{
    int fst = *(int*)i1;
    int scd = *(int*)i2;
    return (fst > scd) ? 1 : (fst < scd) ? -1 : 0;
}

char *test_set_subset()
{
    int superData[] = {1, 2, 3, 4, 5, 6, 7, 8, 10};
    int sub1Data[] = {3, 5, 8, 10};
    int sub2Data[] = {2, 4, 7};
    int notSubData[] = {3, 5, 9};
    Set *superSet = set_create(compare_ints, sizeof(int));
    Set *sub1Set = set_create(compare_ints, sizeof(int));
    Set *sub2Set = set_create(compare_ints, sizeof(int));
    Set *notSubSet = set_create(compare_ints, sizeof(int));
    set_insert_all(superSet, superData, 9);
    set_insert_all(sub1Set, sub1Data, 4);
    set_insert_all(sub2Set, sub2Data, 3);
    set_insert_all(notSubSet, notSubData, 3);
    char result = 0;
    result = set_is_subset_of(sub1Set, superSet);
    mu_assert("first sub set is not a sub set", result);
    result = set_is_subset_of(sub2Set, superSet);
    mu_assert("second sub set is not a sub set", result);
    result = set_is_subset_of(notSubSet, superSet);
    mu_assert("notSubSet is a subset", !result);
    set_free(superSet);
    set_free(sub1Set);
    set_free(sub2Set);
    set_free(notSubSet);
    return 0;
}

char *test_set_equal()
{
    int set1Data[] = {7, 5, 1, 6, 8, 9};
    int set2Data[] = {8, 1, 5, 9, 7, 6};
    int set3Data[] = {1, 5, 6, 8, 7, 10};
    int set4Data[] = {1, 5, 7, 6};
    Set *set1 = set_create(compare_ints, sizeof(int));
    Set *set2 = set_create(compare_ints, sizeof(int));
    Set *set3 = set_create(compare_ints, sizeof(int));
    Set *set4 = set_create(compare_ints, sizeof(int));
    set_insert_all(set1, set1Data, 6);
    set_insert_all(set2, set2Data, 6);
    set_insert_all(set3, set3Data, 6);
    set_insert_all(set4, set4Data, 4);
    char result = 0;
    result = set_is_equal(set1, set2);
    mu_assert("set1 and set2 are not equal", result);
    result = set_is_equal(set1, set3);
    mu_assert("set1 and set3 are equal", !result);
    result = set_is_equal(set1, set4);
    mu_assert("set1 and set4 are equal", !result);
    set_free(set1);
    set_free(set2);
    set_free(set3);
    set_free(set4);
    return 0;
}

char *all_set_tests()
{
    show_testheader("set");
    mu_run_test("testing subset", test_set_subset);
    mu_run_test("testing is equal", test_set_equal);
    return 0;
}