#include "minunit.h"
#include "red_black_tree.h"
#include "vector.h"
#include <stdio.h>

static char compare_ints(void *i1, void *i2)
{
    int fst = *(int*)i1;
    int scd = *(int*)i2;
    return (fst > scd) ? 1 : (fst < scd) ? -1 : 0;
}

static char *test_rbt_inorder_iterator()
{
    RedBlackTree *tree = red_black_tree_create(sizeof(int), compare_ints);
    int i[] = {1, 2, 3, 4, 5, 6};
    red_black_tree_insert(tree, &i[3]);
    red_black_tree_insert(tree, &i[0]);
    red_black_tree_insert(tree, &i[5]);
    red_black_tree_insert(tree, &i[2]);
    red_black_tree_insert(tree, &i[1]);
    red_black_tree_insert(tree, &i[4]);
    RBTInorderIterator *it = rbt_inorder_iterator_create(tree);
    RedBlackTreeNode *current;
    size_t index = 0;
    while((current = rbt_inorder_iterator_get(it)) != NULL)
    {

        mu_assert("not the correct element", *(int*)current->data == index + 1);
        rbt_inorder_iterator_next(it);
        index++;
    }
    free(it);
    red_black_tree_free(tree);
    return 0;
}

static char *test_rbt_insertion()
{
    RedBlackTree *tree = red_black_tree_create(sizeof(int), compare_ints);
    int i[] = {1, 5, 7, 10, 15, 24};
    red_black_tree_insert(tree, &i[2]);
    mu_assert("tree root is not 7", *(int*)tree->root->data == 7);
    red_black_tree_insert(tree, &i[3]);
    mu_assert("tree root is not 7", *(int*)tree->root->data == 7);
    mu_assert("tree roots right child is not 10", *(int*)tree->root->right->data == 10);
    red_black_tree_insert(tree, &i[4]);
    mu_assert("tree root is not 10", *(int*)tree->root->data == 10);
    mu_assert("tree roots right child is not 15", *(int*)tree->root->right->data == 15);
    mu_assert("tree roots left child is not 7", *(int*)tree->root->left->data == 7);
    red_black_tree_insert(tree, &i[1]);
    mu_assert("tree root is not 10", *(int*)tree->root->data == 10);
    mu_assert("tree roots right child is not 15", *(int*)tree->root->right->data == 15);
    mu_assert("tree roots left child is not 7", *(int*)tree->root->left->data == 7);
    mu_assert("tree roots left childs left child is not 5", *(int*)tree->root->left->left->data == 5);
    mu_assert("tree roots left child is not black", tree->root->left->color == RED_BLACK_TREE_BLACK);
    mu_assert("tree roots lright child is not black", tree->root->right->color == RED_BLACK_TREE_BLACK);
    red_black_tree_insert(tree, &i[0]);
    mu_assert("tree root is not 10", *(int*)tree->root->data == 10);
    mu_assert("tree roots right child is not 15", *(int*)tree->root->right->data == 15);
    mu_assert("tree roots left child is not 5", *(int*)tree->root->left->data == 5);
    mu_assert("tree roots left childs left child is not 1", *(int*)tree->root->left->left->data == 1);
    mu_assert("tree roots left childs right child is not 7", *(int*)tree->root->left->right->data == 7);
    mu_assert("tree root is not black", tree->root->color == RED_BLACK_TREE_BLACK);
    mu_assert("tree roots left child is not black", tree->root->left->color == RED_BLACK_TREE_BLACK);
    mu_assert("tree roots lright child is not black", tree->root->right->color == RED_BLACK_TREE_BLACK);
    mu_assert("tree roots left child left child is not red", tree->root->left->left->color == RED_BLACK_TREE_RED);
    mu_assert("tree roots left child right child is not red", tree->root->left->right->color == RED_BLACK_TREE_RED);
    red_black_tree_free(tree);
    return 0;
}


static char *test_rbt_contains()
{
    RedBlackTree *tree = red_black_tree_create(sizeof(int), compare_ints);
    int i[] = {1, 5, 7, 10, 15, 24};
    red_black_tree_insert(tree, &i[3]);
    red_black_tree_insert(tree, &i[0]);
    red_black_tree_insert(tree, &i[5]);
    red_black_tree_insert(tree, &i[2]);
    red_black_tree_insert(tree, &i[1]);
    red_black_tree_insert(tree, &i[4]);
    mu_assert("tree does not contain 1", red_black_tree_contains(tree, &i[0]));
    mu_assert("tree does not contain 5", red_black_tree_contains(tree, &i[1]));
    mu_assert("tree does not contain 7", red_black_tree_contains(tree, &i[2]));
    mu_assert("tree does not contain 10", red_black_tree_contains(tree, &i[3]));
    mu_assert("tree does not contain 15", red_black_tree_contains(tree, &i[4]));
    mu_assert("tree does not contain 24", red_black_tree_contains(tree, &i[5]));
    int x = 17;
    mu_assert("tree does contain 17", !red_black_tree_contains(tree, &x));
    red_black_tree_free(tree);
    return 0;
}

static char *test_rbt_delete()
{
    RedBlackTree *tree = red_black_tree_create(sizeof(int), compare_ints);
    int i[] = {1, 5, 7, 10, 15, 24};
    red_black_tree_insert(tree, &i[3]);
    red_black_tree_insert(tree, &i[0]);
    red_black_tree_insert(tree, &i[5]);
    red_black_tree_insert(tree, &i[4]);
    red_black_tree_insert(tree, &i[1]);
    red_black_tree_insert(tree, &i[2]);
    int x = 29;
    char res = red_black_tree_delete(tree, &x);
    mu_assert("something was wrongly deleted", res == 0);
    res = red_black_tree_delete(tree, &i[1]);
    mu_assert("root->left is not 1", *(int*)tree->root->left->data == 1);
    mu_assert("root->left->parent is not tree root", tree->root == tree->root->left->parent);
    mu_assert("root->left->right is not 7", *(int*)tree->root->left->right->data == 7);
    red_black_tree_free(tree);
    return 0;
}

char* int_to_string(void *i)
{
    char *res;
    size_t len;
    FILE *stream = open_memstream(&res, &len);
    int _i = *(int*)i;
        if(_i < 0)
    fprintf(stream, "\"%d\"", _i);
        else
    fprintf(stream, "%d", _i);
    fclose(stream);
    return res;
}

static char *test_dummy_for_printing()
{
    RedBlackTree *tree = red_black_tree_create(sizeof(int), compare_ints);
    int i[] = {1, 5, 7, 10, 15, 24, 17, -5, 36, 76, 21, 12, 18, 50};
    red_black_tree_insert(tree, &i[11]);
    red_black_tree_insert(tree, &i[3]);
    red_black_tree_insert(tree, &i[5]);
    red_black_tree_insert(tree, &i[10]);
    red_black_tree_insert(tree, &i[12]);
    red_black_tree_insert(tree, &i[1]);
    red_black_tree_insert(tree, &i[7]);
    red_black_tree_insert(tree, &i[13]);
    red_black_tree_insert(tree, &i[2]);
    red_black_tree_insert(tree, &i[8]);
    red_black_tree_insert(tree, &i[9]);
    red_black_tree_insert(tree, &i[0]);
    red_black_tree_insert(tree, &i[6]);
    red_black_tree_insert(tree, &i[4]);
    char *res = red_black_tree_to_mermaid_string(tree, int_to_string);
    printf("%s", res);
    free(res);
    red_black_tree_delete(tree, &i[8]);
    res = red_black_tree_to_mermaid_string(tree, int_to_string);
    printf("%s", res);
    free(res);
    red_black_tree_delete(tree, &i[10]);
    res = red_black_tree_to_mermaid_string(tree, int_to_string);
    printf("%s", res);
    free(res);
    red_black_tree_delete(tree, &i[6]);
    res = red_black_tree_to_mermaid_string(tree, int_to_string);
    printf("%s", res);
    free(res);
    red_black_tree_delete(tree, &i[11]);
    res = red_black_tree_to_mermaid_string(tree, int_to_string);
    printf("%s", res);
    free(res);
    red_black_tree_delete(tree, &i[12]);
    res = red_black_tree_to_mermaid_string(tree, int_to_string);
    printf("%s", res);
    free(res);
    red_black_tree_free(tree);
    return 0;
}


char *all_rbt_tests()
{
    show_testheader("red black tree");
    mu_run_test("testing insertion", test_rbt_insertion);
    mu_run_test("testing contains", test_rbt_contains);
    mu_run_test("testing delete", test_rbt_delete);
    mu_run_test("testing inorder iterator", test_rbt_inorder_iterator);
    printf("\n");
    return 0;
}
