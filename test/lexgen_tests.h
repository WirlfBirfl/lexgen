#ifndef LEXGEN_TESTS_H
#define LEXGEN_TESTS_H

char *all_nfa_tests();

char *all_vector_tests();

char *all_kvs_tests();

char *all_rbt_tests();

char *all_set_tests();

#endif